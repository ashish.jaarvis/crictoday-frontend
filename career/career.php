<?php 
include_once '../apis/apis.php';
$job_slug = $_REQUEST['job_slug'];
$joblist = new Apis('https://cms.crictoday.com/api/joblist?job_slug='.$job_slug);
$joblist = $joblist->getData();

$slug = 'career/'.$job_slug;
$metatitle = $joblist[0]->post_name;
$metadesc = $joblist[0]->post_description;
$metakeywords = 'joblist, career, job';

include_once '../header.php';
?>
<div class="job-detail-page">
<div class="job-title">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col">
					<?php echo $joblist[0]->post_name; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="page-content">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="job-description">
						<?php echo $joblist[0]->post_description; ?>
					</div>
				</div>
				<div class="col-50 fl">
					<div class="short-title"><?php echo $language[0]->primaryres; ?></div>
					<div class="content-p">
						<?php echo $joblist[0]->primary_responsibility; ?>
					</div>
					<div class="short-title"><?php echo $language[0]->keyskillsa; ?></div>
					<div class="content-p">
						<?php echo $joblist[0]->key_skills; ?>
					</div>
				</div>
				<div class="col-50 fl">
					<div class="career-form">
						<div class="short-title"><?php echo $language[0]->submitinfo; ?></div>
						<form name="careerform" id="careerform" autocomplete="off" novalidate enctype="multipart/form-data">
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->uploadres; ?></label></div>
							<div class="career-input fl customfile"><input class="uploadfield" type="file" name="comming_image" id="comming_image" base-sixty-four-input required maxsize="500" accept=".docx,.pdf">
							<label for="comming_image" class="uploadlabel"></label></div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->fullname; ?><span>*</span></label></div>
							<div class="career-input fl"><input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="name" id="name">
							</div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->email; ?><span>*</span></label></div>
							<div class="career-input fl"><input type="email"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="email" id="email">
						</div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->mobilenumber; ?><span>*</span></label></div>
							<div class="career-input fl"><input type="text"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="mobile" id="mobile"  maxlength="10" minlength="10"></div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->relexp; ?><span>*</span></label></div>
							<div class="career-input fl">
								<div class="devide fl">
									<select name="relevant_exp_year" id="relevant_exp_year" class="selectbox" required="">
										<option value="0">Years</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select>
								</div>
								<div class="devide fr">
									<select name="relevant_exp_month" id="relevant_exp_month" class="selectbox" required="">
										<option value="0">Months</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
										<option value="13">13</option>
										<option value="14">14</option>
										<option value="15">15</option>
										<option value="16">16</option>
										<option value="17">17</option>
										<option value="18">18</option>
										<option value="19">19</option>
										<option value="20">20</option>
									</select>
								</div>
								<div class="clr"></div>
							</div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->currentorg; ?><span>*</span></label></div>
							<div class="career-input fl"><input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" required="" name="current_org" id="current_org">
							</div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl"><label class="labs"><?php echo $language[0]->socialpro; ?><span class="no-required">(<?php echo $language[0]->optional; ?>)</span></label></div>
							<div class="career-input fl"><input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="social_dribble" id="social_dribble" placeholder="Enter your Dribbble URL"></div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl">&nbsp;</div>
							<div class="career-input fl"><input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="behance" id="behance" placeholder="Behance"></div>
							<div class="clr"></div>
						</div>
						<div class="form-row">
							<div class="career-label fl">&nbsp;</div>
							<div class="career-input fl"><input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" name="website" id="website" placeholder="Website"></div>
							<div class="clr"></div>
						</div>
						<div id="showCommentErrors" class="form-row"></div>
						<div class="form-row">
							<div class="career-label fl">&nbsp;</div>
							<input type="hidden" name="job_code" value="<?php echo $joblist[0]->job_code; ?>">
							<div class="career-input fl"><button class="button-w" type="button" onclick="careerfun()"><span><?php echo $language[0]->applynow; ?></span></button></div>
							<div class="clr"></div>
						</div>
						</form>
						<div class="career-loader"></div>
					</div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</div>
</div>
<script type="text/javascript">
	careerfun = function(){
	   	$(".career-loader").show();
	   	var formData = new FormData();
		var imagefile = document.querySelector('#comming_image');
		formData.append("image", imagefile.files[0]);
		var obj = $('#careerform').serializeObject();
		$.each(obj, function(k, v){
			formData.append(k, v);
		});

	   		axios.post(siteurl+'jobpost',formData, { 
			 headers: { 'Content-Type': 'multipart/form-data' }
			})
			.then(function(){
				$(".career-loader").show();
	   				setTimeout(function() {
					$(".career-loader").hide();	
					$('form')[0].reset();
					$(".popup-thank.submitapplication").show();
				},4000);
			})
			.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showCommentErrors').html(errorsHtml);
			});
	   }
</script>
<?php include_once '../footer.php'; ?>