<?php 
include_once '../apis/apis.php';
$joblist = new Apis('https://cms.crictoday.com/api/joblist');
$joblist = $joblist->getData();
$slug = 'career';
$metatitle = $language[0]->smallteam;
$metadesc = $language[0]->hereistxt.' '.$language[0]->hereistxt1;
$metakeywords = 'joblist, career, job';

include_once '../header.php';
?>
<div>
	<div class="inner-banner">
		<img src="/assets/images/inner-banner/career-banner.jpg" alt="">
		<div class="banner-text">
			<div class="ads-space">
				<div class="container">
					<div class="row">
						<div class="col">
							<h3 class="center"><?php echo $language[0]->smallteam; ?>.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="define-us">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="title-base"><?php echo $language[0]->hereis; ?>.</div>
						<div class="us-content">
							<?php echo $language[0]->hereistxt; ?>
							<br>

							<?php echo $language[0]->hereistxt1; ?>
						</div>
						<div class="images-block">
							<div class="image-1 fl"><img src="/assets/images/career/image1.jpg"></div>
							<div class="image-2 fl"><img src="/assets/images/career/image2.jpg" class="top"><img src="/assets/images/career/image4.jpg"></div>
							<div class="image-3 fl"><img src="/assets/images/career/image3.jpg" class="top"><img src="/assets/images/career/image5.jpg" alt=""></div>
							<div class="clr"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="current-positions">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="title-base"><?php echo $language[0]->openpos; ?></div>
							

						<p><?php echo $language[0]->openposheadline; ?> </p>
						
						<br>

						<?php
						if(count($joblist) > 0){
							foreach ($joblist as $job) {
								
									echo '<div class="openings-repeat fl">
										<div class="job-title">'.$job->post_name.'</div>
										<div class="job-short-des">'.$job->post_short_desc.'</div>
										<div class="view-job-link"><a href="/career/'.$job->job_slug.'">'.$language[0]->moreinfo.'</a></div>
									</div>';
							}
							echo '<div class="clr"></div>';
						} else{
							echo '<div class="clr"></div><div class="row ng-hide"><div class="col"><div class="not-found">No jobs are posted right now</div></div></div>';
						}
						?>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once '../footer.php'; ?>