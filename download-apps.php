<?php 
	include_once 'apis/apis.php'; 
	$slug = 'download-apps';
	$download = new Apis('https://cms.crictoday.com/api/testimonials?locale='.$lang);
	$download = $download->getData();

	$metatitle = $language[0]->getapp;
	$metadesc = $language[0]->getappcontent;
	$metakeywords = 'download app, crictoday';
	
	include_once 'header.php';
?>
<div class="inner-banner download-bg">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col-50 col-full-apps fl download-left">
					<img src="assets/images/inner-banner/mobile.png" alt="">
				</div>
				<div class="col-50 col-full-apps fl">
					<h4 class="page-title"><?php echo $language[0]->getapp; ?></h4>
					<p><?php echo $language[0]->getappcontent; ?>.</p>
					<div class="download-icon2">
						<a href="#" class="android"></a>
						<a href="#" class="ios"></a>
					</div>
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</div>

<div class="inner-page download-app">
	<div class="ads-space">
		<div class="container">
			<div class="row ">
				<div class="col">
					<h5><?php echo $language[0]->whatourcustomer; ?></h5>
				</div>
			</div>
			<div class="row col-rating">
				<?php
					if (is_array($download) && count($download) > 0) {
						foreach ($download as $key => $testimonial) {
							echo '<div class="col-33 fl">
									<div class="name">'.$testimonial->person_name.'</div> <div class="rating">';
									for($i = 1; $i<= $testimonial->rating; $i++){
										echo '<span></span>';
									}
									echo '</div><p>'.$testimonial->content.'</p>
								</div>';
						}
					}else{
						echo '<div class="white-bg comm-box">No testimonials found.</div>';
					}
				?>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</div>

<?php include_once 'footer.php'; ?>