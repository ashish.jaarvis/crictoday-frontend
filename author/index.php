<?php 
include_once '../apis/apis.php'; 
	$slug = isset($_GET['author_slug']) ? $_GET['author_slug'] : null;
	$page = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=author&author_slug='.$slug);
	$page = $page->getData();
include_once '../header.php';
?>
<div class="home-container ads-space">
	<div class="container">
		<div class="row">

			<div class="col">
				<div class="author-data-get">
					<div class="author-image-at-get fl"><img src="<?php echo $cms.$page->author_data->image; ?>" alt=""></div>
					<div class="author-name-at-get fl"><?php echo $page->author_data->name; ?> <span>Total <?php echo count($page->list); ?> Posts</span>
						<div class="author_description"><?php echo $page->author_data->description; ?></div>
						</div>
					<div class="clr"></div>
				</div>
			</div>
			<div class="clr"></div>
			<div class="col-70 fl">

				<?php 

					if (count($page->list) > 0 ) {
						echo '<div id="listed-cats-news">';
							foreach ($page->list as $key=>$listing) {
								if($key < 10){	
									echo '<div class="listed-news"><div class="white-bg">
											<div class="common-state blue-stage no-hover">
											
												<div class="news-inner">
												<amp-img src="'.$cms.$listing->image.'" width="285" height="161" layout="fixed" alt="'.$listing->category.'"></amp-img>
													<div class="device-left">
													<div class="cat-title fl">'.$listing->category.'</div>
													<div class="cat-tag fr">'.$listing->super_category.'</div>
													<div class="clr"></div>
													<h4><a href="/detail/'.$listing->content_slug.'">'.$listing->title.'</a></h4>
													<div class="author-caption no-break">'.$listing->author.'<span>'.$listing->created_at.'</span></div>
													</div>
													<div class="clr"></div>
												</div>	
											
											</div>
										</div></div>';
									}else{
										break;
									}
								}
						echo '</div>';
						$total = ceil(count($page->list)/10);
						if ($total > 0) {

							echo '<ul class="pagination ng-scope" id="catnewspagination">
								<li class="ng-scope"><a onclick="return categotopage(0, this);">«</a></li>
								<li class="ng-scope"><a onclick="return categotoprevpage();">‹</a></li>';
								$j = 0;
								for ($i = 1; $i <= $total; $i++)
								{
								    if ($i == 1)
								    {
								        echo '<li class="ng-scope page-number active"><a id="' . $j . '" onclick="return categotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
								    }
								    else
								    {
								        echo '<li class="ng-scope page-number"><a id="' . $j . '" onclick="return categotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
								    }
								    $j++;
								}
							echo '<li class="ng-scope"><a onclick="return categotonextpage();">›</a></li>
								<li class="ng-scope"><a onclick="return categotopage(4, this);">»</a></li>
							</ul>';
						}	

					}else{
						echo '<div class="white-bg comm-box">No Record Found.</div>';
					}
				?>
		
			</div>
			<div class="col-30 fr">
				<div class="widget">
					<div class="white-bg">
						<div class="result-tab">
							<a href="javascript:void(0)" class="active" id="live">Live</a>
							<a href="javascript:void(0)" id="results">Results</a>
							<a href="javascript:void(0)" id="fixtures">Fixtures</a>
							<div class="clr"></div>
						</div>
						<div class="result-container content-live">
							<div class="repeat-result">
								<b>4th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>4th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>4th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
						</div>
						<div class="result-container content-results">
							<div class="repeat-result">
								<b>5th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>5th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>5th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
						</div>
						<div class="result-container content-fixtures">
							<div class="repeat-result">
								<b>6th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>6th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
							<div class="repeat-result">
								<b>6th ODI: West Indies v India at North Sound</b> - Jul 2, 2017
								<span>West Indies won by 11 runs</span>
							</div>
						</div>
					</div>
				</div>
				</div>
				<?php //include_once '../common/specials.php' ?>
			<div class="clr"></div>		
	
		</div>
	</div>
</div>
<script type="text/javascript">
	/*********************Category List Pagination*****************/
$(window).load(function(){
	var currPage = localStorage.getItem('catpage');
	//$('#catnewspagination li a#'+currPage).trigger('click');
});	
function catgetposts(page, perpage, offset, ele){
	let newslist = '';
	var apiurl = 'https://cms.crictoday.com/api/content-list?locale=<?php echo $lang; ?>&type=author&author_slug=<?php echo $slug; ?>&limit=10&offset='+offset;
	axios.get(apiurl)
	  .then(function (response) {
	    if (response.data.list.length > 0) {
	    	response.data.list.map(news => {
	    		newslist +='<div class="listed-news"><div class="white-bg"><div class="common-state sky-stage no-hover"><div class="news-inner"><amp-img srcset="https://cms.crictoday.com/'+news.image+'" src="https://cms.crictoday.com/'+news.image+'" alt="'+news.title+'" width="285" height="161" layout="fixed"></amp-img><div class="device-left"><div class="cat-title fl">'+news.category+'</div><div class="cat-tag fr">'+news.super_category+'</div><div class="clr"></div><h4><a href="/detail/'+news.content_slug+'">'+news.title+'</a></h4><div class="author-caption no-break">'+news.author+'<span>'+news.created_at+'</span></div></div><div class="clr"></div></div></div></div></div>';
	    	});
	    }
	    jQuery('#listed-cats-news').html(newslist);
	    jQuery('#catnewspagination .ng-scope').removeClass('active');
	    jQuery(ele).parent().addClass('active');
	    localStorage.setItem("catpage", page);
	  })
	  .catch(function (error) {
	    // handle error
	    console.log(error);
	  })
}

function categotopage(page, ele){
	var perpage = 10;
	var offset = page*perpage;
		var ele = ele;
	if (page == '0') {
		ele = jQuery('#catnewspagination li:eq(2) a');
	}if (page == '4') {
		ele = jQuery('#catnewspagination li:eq(6) a');
	}
	catgetposts(page, perpage, offset, ele);
	 
}
function categotoprevpage(){
	var getcurrpage = parseInt(jQuery('#catnewspagination .ng-scope.active a').attr('id'));

	if (getcurrpage>0) {
		var page = getcurrpage-1;
		var perpage = 10;
	    var offset = page*perpage;
	    var prevpage = jQuery('#catnewspagination li:eq('+eval(getcurrpage+1)+') a');
	    catgetposts(page, perpage, offset, prevpage);
	}
}
function categotonextpage(){
	var getcurrpage = parseInt(jQuery('#catnewspagination .ng-scope.active a').attr('id'));

	if (getcurrpage<4) {
		var page = getcurrpage+1;
		var perpage = 10;
	    var offset = page*perpage;
	    var nextpage = jQuery('#catnewspagination li:eq('+eval(getcurrpage+3)+') a');

	    catgetposts(page, perpage, offset, nextpage);
	}
}
</script>
<?php include_once '../footer.php'; ?>