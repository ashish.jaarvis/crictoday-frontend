<?php
include_once 'apis/apis.php';
$slug = 'quiz';
$quiz = new Apis('https://cms.crictoday.com/api/quiz-detial?locale='.$lang);
$quiz= $quiz->getData();

$metatitle = $quiz[0]->quiz_title;
$metadesc = $quiz[0]->question;
$metakeywords = 'quiz, crictoday';

include_once 'header.php';
?>
<div class="quizpage">
  <div class="ads-space">
    <div class="container">
      <div class="row news-detail">
        <div class="col blue-stage no-hover">
          <h4 class="page-title margin-botton20 ng-binding"><?php echo $quiz[0]->quiz_title; ?></h4>
        </div>
        <div class="col-50 share-col col-xs-full fl"> <span>Share:</span> 
          <!-- ngIf: engagement --> 
          <div class="sharethis-inline-share-buttons"></div> </div>
        <div class="col-50 share-col  datshare col-xs-full fr"> <?php echo $quiz[0]->quiz_date; ?> </div>
        <div class="clr"></div>
      </div>
    </div>
  </div>
  <div class="ads-space">
    <div class="container">
      <div class="row news-detail quiz-detail">
        <ul class="quizpagination">
          <?php
            foreach ($quiz as $key=> $q) {
              if ($key == 0) {
                echo '<li id="qus-'.$key.'" class="on"> <span class="act">'.$q->count.'</span> </li>';
              }else{
                echo '<li id="qus-'.$key.'"> <span class="act">'.$q->count.'</span> </li>';
              }
            }
          ?>
        </ul>

        <?php
            foreach ($quiz as $k => $q) {
            if ($k == 0) {
              echo '<div class="repeatquestions active" id="qus-'.$k.'">
            
             <a href="javascript:void(0)" onclick="selectContinue('.$k.')" class="nxtques ">Next Question</a>          

          <h2>'.$q->question.'</h2>
          <div class="quizimg">';
              foreach ($q->ans as $ans) {
                echo '<div class="ngquiz" id="q'.$k.$ans->id.'" onclick="selectAnswer(this, '.$k.','.$ans->id.', '.$q->correct.')">
             
              <label> <span></span> <strong>'.$ans->title.'</strong></label>
              <img src="'.$ans->image_path.'" alt=""/> </div>';
              }
      
          echo '<div class="clr"></div>
          </div>
        </div>';
            }else{
              echo '<div class="repeatquestions" id="qus-'.$k.'">
            
             <a href="javascript:void(0)" onclick="selectContinue('.$k.')" class="nxtques ">Next Question</a>          

          <h2>'.$q->question.'</h2>
          <div class="quizimg">';
              foreach ($q->ans as $ans) {
                echo '<div class="ngquiz" id="q'.$k.$ans->id.'" onclick="selectAnswer(this,'.$k.' ,'.$ans->id.', '.$q->correct.')">
             
              <label> <span></span> <strong>'.$ans->title.'</strong></label>
              <img src="'.$ans->image_path.'" alt=""/> </div>';
              }
      
          echo '<div class="clr"></div>
          </div>
        </div>';
            }
      }
        ?>
      <div class="results inactive">
        <h3>Results</h3>
        <p>You scored <span id="percnt"></span>% by correctly answering <span id="ansQ"></span> of the total <span id="totalQus"></span> questions.</p>
        <a class="playagain" href="javascript:void(0)" onclick="playagain()">Play Again</a>        
      </div>
      </div>
      <div class="clr"></div>
    </div>
  </div>
</div>
<!--end wrppaer-->
<script type="text/javascript">
  var quiz;
  var score = 0;
  var activeQuestion = 0;
  var activeQuestionAnswered = 0;
  var percentage = 0;
  var totalQuestions = 0;
  
  axios.get(siteurl+'quiz-detial?locale=<?php echo $lang; ?>').then(function(res) {
    quiz = res.data;
    totalQuestions = quiz.length;
        }, function(err) {
            console.log(err);
  });

  selectAnswer = function(ele,qnum, qIndex, aIndex){
    $(ele).addClass('selected');
    $('.repeatquestions#qus-'+qnum).addClass('answered');
    var correctAnswer = quiz[qnum].correct;
    if (correctAnswer === aIndex) {
      $('.repeatquestions.active .ngquiz#q'+qnum+aIndex).addClass('correct');
    }
    if(qIndex === aIndex){
      $('.quizpagination #qus-'+qnum).removeClass('on').addClass('off').addClass('correct').addClass('answered');
    }
    else{
      $('.quizpagination #qus-'+qnum).removeClass('on').addClass('off').addClass('incorrect').addClass('answered');
    }
      if (qIndex === aIndex) {
        score += 1;
      }
      percentage = ((score / totalQuestions)*100).toFixed(2);
    }
    selectContinue = function(next){
      if (next < eval(totalQuestions-1)) {
        $('.repeatquestions#qus-'+next).addClass('inactive').removeClass('active');
        $('.quizpagination #qus-'+next).removeClass('on');
        next = next + 1;
        $('.quizpagination #qus-'+next).addClass('on');
        $('.repeatquestions#qus-'+next).addClass('active');
      }else{
        $('.repeatquestions#qus-'+next).addClass('inactive').removeClass('active');
        $('#percnt').text(percentage);
        $('#ansQ').text(score);
        $('#totalQus').text(totalQuestions);
        $('.results').removeClass('inactive').addClass('active');
      }      
    }

playagain = function(){
      return location.reload();
    }
</script>
<?php include_once 'footer.php'; ?>