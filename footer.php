<?php include_once 'common/common-footer.php'; ?>
<script type="text/javascript">
jQuery(function ($) {
    var currentHash = $('.first-row').attr("id");
    $.fn.isInViewport = function() {
		  var elementTop = $(this).offset().top;
		  var elementBottom = elementTop + $(this).outerHeight();

		  var viewportTop = $(window).scrollTop();
		  var viewportBottom = viewportTop + $(window).height();

		  return elementBottom > viewportTop && elementTop < viewportBottom;
		};

		$(window).on('resize scroll', function() {
		  $('.news-row').each(function() {
		      var activeColor = $(this).attr('id');
		    if ($(this).isInViewport()) {
		      window.history.replaceState({},activeColor,activeColor);
		    }
		  });
		});
});

	jQuery(document).ready(function($) {
      $('.home-slider').slick({
        infinite: true,
		  slidesToShow: 1,
		  autoplay: true,
		  slidesToScroll: 1,
		  dots:true,
		  responsive:[
		  {
		  	breakpoint:1365,
		  	settings:{
		  		arrows: false
		  	}
		  }				  
		  ]
    });
    $('.video-slider-home').slick({
        infinite: true,
		  slidesToShow: 3,
		  autoplay: true,
		  slidesToScroll: 1,
		  dots:true,
		  arrows: false,
		  responsive:[
		  {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		  {
		  	breakpoint:767,
		  	settings:{
		  		slidesToShow: 1,
		  	}
		  }				  
		  ]
    });  
});
	/*********************News List Pagination*****************/
$(window).load(function(){
	var currPage = localStorage.getItem('homepage');
	//$('#newspagination li a#'+currPage).trigger('click');
});		
function gotopage(page, ele){
	var perpage = 10;
	var offset = page*perpage;
		var ele = ele;
	if (page == '0') {
		ele = jQuery('#newspagination li:eq(2) a');
	}if (page == '4') {
		ele = jQuery('#newspagination li:eq(6) a');
	}
	getposts(page, perpage, offset, ele);
	 
}
function gotoprevpage(){
	var getcurrpage = parseInt(jQuery('#newspagination .ng-scope.active a').attr('id'));

	if (getcurrpage>0) {
		var page = getcurrpage-1;
		var perpage = 10;
	    var offset = page*perpage;
	    var prevpage = jQuery('#newspagination li:eq('+eval(getcurrpage+1)+') a');
	    getposts(page, perpage, offset, prevpage);
	}
}
function gotonextpage(){
	var getcurrpage = parseInt(jQuery('#newspagination .ng-scope.active a').attr('id'));

	if (getcurrpage<4) {
		var page = getcurrpage+1;
		var perpage = 10;
	    var offset = page*perpage;
	    var nextpage = jQuery('#newspagination li:eq('+eval(getcurrpage+3)+') a');
	    console.log(page, getcurrpage, offset, nextpage);
	    
	    getposts(page, perpage, offset, nextpage);
	}
}
function getposts(page, perpage, offset, ele){
	let newslist = '';
	var apiurl = 'https://cms.crictoday.com/api/content-list?locale=en&type=homelisting&limit=10&offset='+offset;
	axios.get(apiurl)
	  .then(function (response) {
	    if (response.data.length > 0) {
	    	response.data.map(news => {
	    		newslist +='<div class="listed-news"><div class="white-bg"><div class="common-state sky-stage no-hover"><div class="news-inner"><amp-img srcset="https://cms.crictoday.com/'+news.image+'" src="https://cms.crictoday.com/'+news.image+'" alt="'+news.title+'" width="285" height="161" layout="fixed"></amp-img><div class="device-left"><div class="cat-title fl">'+news.category+'</div><div class="cat-tag fr">'+news.super_category+'</div><div class="clr"></div><h4><a href="/detail/'+news.content_slug+'">'+news.title+'</a></h4><div class="author-caption no-break">'+news.author+'<span>'+news.created_at+'</span></div></div><div class="clr"></div></div></div></div></div>';
	    	});
	    }
	    localStorage.setItem("homepage", page);
	    jQuery('#newslist').html(newslist);
	    jQuery('#newspagination .ng-scope').removeClass('active');
	    jQuery(ele).parent().addClass('active');
	  })
	  .catch(function (error) {
	    // handle error
	    console.log(error);
	  })
	  .finally(function () {
	    // always executed
	  });
}
</script>
</body>
</html>