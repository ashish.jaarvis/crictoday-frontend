<?php
include_once 'apis/apis.php'; 
$slug = 'about-us';
$aboutus = new Apis('https://cms.crictoday.com/api/page-detail?locale='.$lang.'&page_slug=about-us');
$aboutus = $aboutus->getData();

$metatitle = $aboutus[0]->name;
$metadesc = $aboutus[0]->content;
$metakeywords = $aboutus[0]->meta_keywords;

include_once 'header.php';
?>
<div>
	<div class="static-content">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="page-title"><?php echo $aboutus[0]->name; ?></h3>
					</div>
					<div class="col"><?php echo $aboutus[0]->content; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once 'footer.php'; ?>