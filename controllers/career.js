app.controller("career", function ($scope, $http) {	  
   $http.get(siteurl+"joblist")
      .success(function(response){
      	$scope.joblist = response;
      	$(".loader").hide();
    })
    if(localStorage.lang=="hi"){
    	$http.get(langurl+"hi.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
    else{
    	$http.get(langurl+"en.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
});
// app.directive("fileInput", function ($parse) {
// 	return{
// 		link: function($scope, element, attrs){
// 			element.on("change", function(event){
// 				var files = event.target.files;
// 				console.log(files[0].name);
// 				$scope.career.resume_path = "http://abhishek.com/"+files[0].name;
// 				$parse(attrs.fileInput).assign(element[0].files);
// 				$scope.$apply();
// 			})
// 		}
// 	}
// })
app.controller("job-detail", function ($scope, $http, $routeParams ,$rootScope) {
	if(localStorage.lang=="hi"){
    	$http.get(langurl+"hi.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
    else{
    	$http.get(langurl+"en.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
      $scope.job_slug = $routeParams.job_slug ;
      $http.get(siteurl+"joblist")
		.success(function(response){			
			$scope.singlejobres = response;	
			$(".loader").hide();		
			//////singleresponse//////
	      	$scope.singlejob = $($scope.singlejobres).filter(function (i,n){
				return n.job_slug === $scope.job_slug;
			});	
			$scope.job_title = $scope.singlejob[0].post_name;		
			$scope.job_description = $scope.singlejob[0].post_description;
			$scope.primary_responsiblity = $scope.singlejob[0].primary_responsibility;
			$scope.key_skills = $scope.singlejob[0].key_skills;	
			$scope.career.job_code = $scope.singlejob[0].job_code;
			document.querySelector('title').innerHTML = $scope.job_title+ "- Career - Crictoday";	
	   })	
	   $scope.career = {}; 	 
	   $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {
			$scope.career.resume_path = fileObj;
	   };
	   $scope.career.social_dribble = "";
	   $scope.career.behance = "";
	   $scope.career.website = "";   
	   $scope.careerfun = function(){
	   	$(".career-loader").show();
	   			$http({
			            method: 'POST',
			            url: siteurl+'jobpost',
			            data: JSON.stringify($scope.career),
			            headers: { 'Content-Type': undefined }
			    })
			    .then(function successCallback(data, status, headers, config) {
	   				$(".career-loader").show();
	   				setTimeout(function() {
						$(".career-loader").hide();	
						$('form')[0].reset();
						 //$(".popup-message-text").html(data.mess);
						 $(".popup-thank.submitapplication").show();
					},4000);
				}, function errorCallback(response) {
					console.log(response)
				})
	   }	
});