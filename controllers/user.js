if(Cookies.get('lang')=="hi"){
    	axios.get(langurl+"hi.json")
	      .then(function(response){
	      	language = response;
	    })
    }
    else{
    	axios.get(langurl+"en.json")
	      .then(function(response){
	      	language = response;
	    })
}

/************************** USER REGISTER ************************************/

   register = {};  
   registerfun = function(){
   			axios.post(siteurl+'user-register',$('#registrationform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function (data, status, headers, config) {
   					$(".popup-register").find(".popup-loader").show();	
		 			$(".popup-register").find(".close").hide(); 		 				
		 			setTimeout(function() {
						$(".popup-register").find(".popup-loader").hide();	
		 				$(".popup-register").find(".close").show(); 
		 				$(".popup-register").hide();
		 				$(".popup-thank.registerthanks").show();
					},2000);
			}).catch(error => {	
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showRegErrors').html(errorsHtml);
			});
   			
   };

/************************** FORGOT PASSWORD ************************************/
 
   forget = {};  
   forgetfun = function(){
		    axios.post(siteurl+'user-forget',$('#forgetform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function (data, status, headers, config) {
				$(".popup-forgot").find(".popup-loader").show();	
	 			$(".popup-forgot").find(".close").hide(); 		 				
	 			setTimeout(function() {
					$(".popup-forgot").find(".popup-loader").hide();	
	 				$(".popup-forgot").find(".close").show(); 
	 				$(".popup-forgot").hide();
	 				$(".popup-thank.forgetthanks").show();
				},4000);
			})
			.catch(error => {	
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showForgetErrors').html(errorsHtml);
			});   			
   };

/**************************Change Password************************************/
   changepasswordfun = function(){
		    axios.post(siteurl+'user-changepassword',$('#changepasswordform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function (data, status, headers, config) {
				$(".popup-changepassword").find(".popup-loader").show();	
	 			$(".popup-changepassword").find(".close").hide(); 		 				
	 			setTimeout(function() {
					$(".popup-changepassword").find(".popup-loader").hide();	
	 				$(".popup-changepassword").find(".close").show(); 
	 				$(".popup-changepassword").hide();
	 				$(".popup-thank.changepassword").show();
				},2000);
			})
			.catch(error => {	
     		var errorsHtml="";
     		var err = error.response.data;
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showChangePasswordErrors').html(errorsHtml);
			});     			
   }




/**************************LOGIN FORM************************************/

	logins = {};
	loginfun = function(){

		var token,username, email;
		axios.post(siteurl+'user-login',$('#loginform').serializeObject(), { 
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
   		.then(function (data) {   				
   			$(".popup-login").find(".popup-loader").show();	
		 	$(".popup-login").find(".close").hide(); 		 				
		 		setTimeout(function() {
					$(".popup-login").find(".popup-loader").hide();	
		 			$(".popup-login").find(".close").show(); 
		 			$(".popup-login").hide();
		 			$("body").removeClass("fixed");
				},2000); 
			$('#showErrors').empty();			
			loginresponse = data;
			token = loginresponse.data.token;
			username = loginresponse.data.username;
			email = loginresponse.data.email;


			Cookies.set('token', token);
			Cookies.set('username', username);
			Cookies.set('email', email);
			window.location.reload(); 
     		}).catch(error => {
     		$(".popup-login").find(".popup-loader").show();	
		 		$(".popup-login").find(".close").hide();
		 		setTimeout(function() {
					$(".popup-login").find(".popup-loader").hide();	
		 			$(".popup-login").find(".close").show();
				},2000);	
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showErrors').html(errorsHtml);
			});
    };

	
	/////////////////////////////GOOGLE LOGIN FUNCTION/////////////////////////////////
	gmail = {
		googlename:"",
		googlemail:""
	}
	googleloginfunc = function(){
		// API call for Google login
			gapi.auth2.getAuthInstance().signIn().then(
				// On success
				function(success) {
					// API call to get user information
					gapi.client.request({ path: 'https://www.googleapis.com/plus/v1/people/me' }).then(
						// On success
						function(success) {
							var token = gapi.auth2.getAuthInstance().currentUser.get().getAuthResponse().id_token;
							console.log(token);
							var user_info = JSON.parse(success.body);
							console.log(user_info);
								$apply(function(){
								///////geting google response to your setted variable ////////	
								gmail.googlename = user_info.displayName;
								gmail.googlemail = user_info.emails[0].value;
								g_image = user_info.image.url;
								token = "651sd6a1sdasdakojsaflkaslabsfo";
								///////// storing items to local storage/////////////
								Cookies.set('plusname', gmail.googlename);
								Cookies.set('plusmail', gmail.googlemail);
								Cookies.set('plusimage', g_image);
								Cookies.set('token', token);
								//////////storing to rootscope to use globally/////////
								//plusimage = g_image;
								//plusname = googlename;
								//plusmail = googlemail;
								//view_token = token;
								//////////////////////////////////////////////////////

								$(".popup-login").hide();
								$("body").removeClass("fixed");

								window.location.reload();


							})								
						},
						// On error
						function(error) {

						}
					);
				},
				// On error
				function(error) {
					alert('Error : Login Failed');
				}
			);
	}

	///////////////////////////facebook////////////////////////////////////////////

facebook = {
		facebookname:"",
		facebookemail:""
	}
	fbloginfunc = function(){
		FB.login(function(response){
			if(response.authResponse){
				FB.api('/me', 'GET', {fields: 'email, first_name, name, id, picture'}, function(response){
					$apply(function(){
						
						///////geting facebook response to your setted variable ////////
						facebook.facebookname = response.name;
						facebook.facebookemail = response.email;
						fb_image = response.picture.data.url;
						token = "f8ace5bf131f32e1a39af4ced5142fde";
						///////// storing items to local storage/////////////
						Cookies.set('fbname', facebook.facebookname);
						Cookies.set('fbmail', facebook.facebookemail);
						Cookies.set('fbimage', fb_image);
						Cookies.set('token', token);
						//////////storing to rootscope to use globally/////////
						//fbimage = fb_image;
						//fbname = facebookname;
						//fbmail = facebookemail;
						//view_token = token;
						//////////////////////////////////////////////////////

						$(".popup-login").hide();
							$("body").removeClass("fixed");
						window.location.reload();

					})
				})
			}
			else{
				// error
			}
		}, {
			scope: 'email, user_likes',
			return_scopes: true
		})
		
	};