app.controller("homepage", function ($scope, $http, $rootScope) {
		$http.get(siteurl+"super-categories"+endurl+localStorage.getItem('lang'))
	      .success(function(response){
	      	$scope.supercat = response;						
	    }) 
		$scope.myPageSize = 2;

		$http.get(siteurl+"content-list?locale="+localStorage.getItem('lang')+"&category_id=14&&type=slider")
		.success(function(response){
	      	$scope.sliderhome = response;
	      	setTimeout(function() {
				$(".home-slider").not('.slick-initialized').slick({
				  infinite: true,
				  slidesToShow: 1,
				  autoplay: true,
				  slidesToScroll: 1,
				  dots:true,
				  responsive:[
				  {
				  	breakpoint:1365,
				  	settings:{
				  		arrows: false
				  	}
				  }				  
				  ]
				})
				$(".loader").hide();
			},2100);
	    })
	    $http.get(siteurl+"content-list?locale="+localStorage.getItem('lang')+"&type=hometoppost4")
		.success(function(response){
	      	$scope.home4 = response;
	    })
	    $http.get(siteurl+"page-detail?locale="+localStorage.getItem('lang')+"&page_slug=breaking-news")
		.success(function(response){
	      	$scope.breaking = response[0].content;
	    })
	    $http.get(siteurl+"content-list?locale="+localStorage.getItem('lang')+"&type=homelisting")
		.success(function(response){
	      	$scope.homelisting = response;
	    })
	    $http.get(siteurl+"content-list?locale="+localStorage.getItem('lang')+"&type=videosall")
			.success(function(response){
				$scope.listvideo = response;
				setTimeout(function() {
				$(".video-slider-home").not('.slick-initialized').slick({
						  infinite: true,
						  slidesToShow: 3,
						  autoplay: true,
						  slidesToScroll: 1,
						  dots:false,
						  responsive:[
						  {
						  	breakpoint:1365,
						  	settings:{
						  		arrows: false,
						  		dots:true
						  	}
						  },
						  {
						  	breakpoint:1023,
						  	settings:{
						  		slidesToShow: 2,
						  		arrows: false,
						  		dots:true
						  	}
						  },
						  {
						  	breakpoint:599,
						  	settings:{
						  		slidesToShow: 1,
						  		arrows: false,
						  		dots:true
						  	}
						  }
						  ]
				})
				$(".inside-bg").not('.slick-initialized').slick({
					  infinite: true,
					  slidesToShow: 1,
					  autoplay: true,
					  slidesToScroll: 1,
					  adaptiveHeight: true, 
					  dots:true,
					  arrows:false
				})
				},2100);
		})
		$http.get(siteurl+"specials-list?locale="+localStorage.getItem('lang')+"&type=trending")
		.success(function(response){
	      	$scope.singletrending = response[0].posts;	      	
	    })	

});  	    





