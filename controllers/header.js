    var dd = {}, $rootScope, langresponsehindi;
    var lang = Cookies.get('lang') ? Cookies.get('lang') : "en";
    var siteurl = "https://cms.crictoday.com/api/";
    var cmsurl = "https://cms.crictoday.com/";
    updatelang = function() {
       dd.languages = "hi";
       axios.post(siteurl + 'get_lang',JSON.stringify(dd),{
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
      })
	  .then(function (data) {
	    langresponsehindi = data.data.languages;
        Cookies.set('lang', langresponsehindi);
        // window.location.href=site_base_url;
        //$route.reload();
       window.location.reload();
	  })
	  .catch(function (error) {
	    console.log(error);
	  });
    }

    getLangJson = function() {
        var flag = "en.json"
        if (lang == "hi") {
            flag = "hi.json";
        }
        axios.get(langurl + flag).then(function(res) {
            language = res.data;
            Cookies.set('language') = JSON.stringify(language);
        }, function(err) {
            console.log(err);
        })
    }

    updatelangeng = function() {
    	dd.languages = "en";
    	axios.post(siteurl + 'get_lang',JSON.stringify(dd),{
	        headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
	      })
		  .then(function (data) {
		    Cookies.remove('lang');
            window.location.href = site_base_url;
		  })
		  .catch(function (error) {
		    console.log(error);
		  });
    }
    inputActive = true;

    searchInput = function(event) {
        var searchValue = event.target.value;
        var searchHtml = "";

        var lang = localStorage.lang ? localStorage.lang : 'en';
        if (searchValue.length >= 3) {
            inputActive = false;
          axios.get(siteurl + 'search?locale=' + lang + '&searchString=' + searchValue,{
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
          })
          .then(function (res) {
            var posts = res.data.posts_all;
            if (posts.length > 0) {
                posts.map( post => {
                    searchHtml += '<div class="repeat-search"><a href="/detail/'+post.content_slug+'"><div class="name fl">'+post.title+'</div> <div class="cat-tag fr">'+post.super_category+'</div><div class="cat-title fr">'+post.category+'</div><div class="clr"></div></a></div>';
                });
            }
            $('#result-search').html(searchHtml);
          })
          .catch(function (error) {
            console.log(error);
          });
        }else{
            $('#result-search').empty();
        }
    };
     logout = function() {
        
        Cookies.remove('token');
        Cookies.remove('username');
        Cookies.remove('email');

        Cookies.remove('plusimage');
        Cookies.remove('plusmail');
        Cookies.remove('plusname');

        Cookies.remove('fbimage');
        Cookies.remove('fbmail');
        Cookies.remove('fbname');



        window.location.reload();

    }

    logoClicked = function() {
        if (window.location.pathname == '/') {
            window.location.reload();
        } else {
            window.location.href = "/"
        }
    }
$(document).ready(function(){
    $('#ham-menu-icon').click(function(event) {
        event.stopPropagation();
        var menu = $('#ham-menu');
        if (menu.hasClass('active')) {
            menu.removeClass('active');
            $(this).removeClass('up');
            $('body').removeClass('fixed');
        } else {
            menu.addClass('active');
            $('body').addClass('fixed');
            $(this).addClass('up');
        }
    })
    
    $('body').click(function(event) {
        if (event.target.id != "ham-menu" && $('#ham-menu').hasClass('active')) {
            $('#ham-menu').removeClass('active');
            $('body').removeClass('fixed');
            $('#ham-menu-icon').removeClass('up');
        }
    });

    $('#ham-menu li span.arrow').on('click', function(e){
        item = e.target.id;
        if (!$("#" + item).hasClass('minsing')) {
            $("#" + item).siblings(".sup_cats").css("height", "210px");
            $("#" + item).siblings(".sup_cats").css("padding", "10px 0px");
            $("#" + item).addClass('minsing');
        } else {
            $("#" + item).siblings(".sup_cats").css("height", "0px");
            $("#" + item).siblings(".sup_cats").css("padding", "0px");
            $("#" + item).removeClass("minsing");
        }
        e.stopPropagation();
    });
});