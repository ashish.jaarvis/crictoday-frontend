app.controller("ipl", function ($scope, $http, $rootScope) {	  
   	$http.get(siteurl+"team?locale="+lang+"&type=ipl")
      .success(function(response){
      	$scope.iplteamlist = response;
      	$(".loader").hide();
    }) 
    if(localStorage.lang=="hi"){
    	$http.get(langurl+"hi.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
    else{
    	$http.get(langurl+"en.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }  
});


app.controller("iplsingle", function ($scope, $http, $routeParams, $rootScope) {
		if(localStorage.lang=="hi"){
    	$http.get(langurl+"hi.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
    else{
    	$http.get(langurl+"en.json")
	      .success(function(response){
	      	$scope.language = response;
	    })
    }
	  $scope.team_slug = $routeParams.team_slug;
		$http.get(siteurl+"team?locale="+lang+"&team_slug="+$scope.team_slug)
	      .success(function(response){
	      	$scope.teamsdata = response;

	      	$scope.teamsdata_players = response[0].team_players;
	      	$scope.teamname = response[0].team_name;
	      	$scope.team_single_logo = response[0].team_logo;
	      	console.log($scope.teamsdata_players)
	      	$(".loader").hide();
	    }) 	
	    $http.get(siteurl+"team?locale="+lang+"&type=IPL")
	      .success(function(response){
	      	$scope.iplteamlist = response;
	      	console.log($scope.iplteamlist)
	      	$(".loader").hide();
	    }) 	
});


