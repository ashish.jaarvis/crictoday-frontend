feedbackfun = function(){
			$('#feedbackform button').attr('disabled', 'disabled');
			axios.post(siteurl+'feedbackpost',$('#feedbackform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function successCallback(data, status, headers, config) {
				$(".form-loader").show();
				setTimeout(function() {
						$(".form-loader").hide();
						$('form')[0].reset();
						$(".popup-thank.fedbacksubmit").show();
					},4000);
				$('#feedbackform button').removeAttr('disabled');
			})
			.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showFeedbackErrors').html(errorsHtml);
     		$('#feedbackform button').removeAttr('disabled');
			});
   			
}
commentsfun = function(){
			$('#commentform button').attr('disabled', 'disabled');
			axios.post(siteurl+'comment-submit',$('#commentform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function successCallback(data, status, headers, config) {
				$(".loader").show();
				$('#comment').val('');
				$(".popup-thank.commentsubmit").show();
				$('#showCommentErrors').empty();
				$('#commentform button').removeAttr('disabled');
			})
			.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showCommentErrors').html(errorsHtml);
     		$('#commentform button').removeAttr('disabled');
			});
	   			
	   }

contactfunc = function(){
			$('#contactform button').attr('disabled', 'disabled');
			axios.post(siteurl+'contact-post',$('#contactform').serializeObject(), { 
			 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
			})
   			.then(function successCallback(data, status, headers, config) {
				$('form')[0].reset();
		   		$(".popup-thank.contactthanks").show();
				$('#showContactErrors').empty();
				$('#contactform button').removeAttr('disabled');
			})
			.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}
			$('#contactform button').removeAttr('disabled');	
     		$('#showContactErrors').html(errorsHtml);
			});
	   			
	   }	   

   subscribefun = function(){
   		$('#subsform button').attr('disabled', 'disabled');

		axios.post(siteurl+'newsletter-submit',$('#subsform').serializeObject(), { 
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
		.then(function successCallback(data, status, headers, config) {
		$(".loader").show();
		setTimeout(function() {
				$(".loader").hide();
				$('form')[0].reset();
				$(".popup-thank.subscribesubmit").show();
			},2000);
		$('#showSubsErrors').empty();
		$('#subsform button').removeAttr('disabled');	
		})
		.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showSubsErrors').html(errorsHtml);
     		$('#subsform button').removeAttr('disabled');	
			});
};

singlesubscribefun = function(){
		$('#singlesubsform button').attr('disabled', 'disabled');
		axios.post(siteurl+'newsletter-submit',$('#singlesubsform').serializeObject(), { 
		 headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		})
		.then(function successCallback(data, status, headers, config) {
		$(".loader").show();
		setTimeout(function() {
				$(".loader").hide();
				$('form')[0].reset();
				$(".popup-thank.subscribesubmit").show();
			},2000);
		$('#showSingleSubsErrors').empty();
		$('#singlesubsform button').removeAttr('disabled');	
		})
		.catch(error => {
     		var errorsHtml="";
     		var err = error.response.data;
     		
     		if(err.status_code === "0" ){	
				if (err.errors) {
				err.errors.map(er => {
				   errorsHtml += '<p class="mr form-error" >'+er+'</p>';
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
     		$('#showSingleSubsErrors').html(errorsHtml);
     		$('#singlesubsform button').removeAttr('disabled');	
			});
};