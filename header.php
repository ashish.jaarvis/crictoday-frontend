<?php
ini_set('display_errors', 0);
//$domain = 'http://'.$_SERVER['SERVER_NAME'].'/crictoday-frontend/';
$token = isset($_COOKIE['token']) ? $_COOKIE['token'] : null;
$username = isset($_COOKIE['username']) ? $_COOKIE['username'] : null;
$email = isset($_COOKIE['email']) ? $_COOKIE['email'] : null;
$plusimage = isset($_COOKIE['plusimage']) ? $_COOKIE['plusimage'] : null;
$fimage = isset($_COOKIE['fimage']) ? $_COOKIE['fimage'] : null;
?>
<!doctype html>
<html amp lang="en">
  <head>
    <meta charset="utf-8">
    <script async src="https://cdn.ampproject.org/v0.js"></script>
    <title><?php echo !empty($metatitle) ? $metatitle.'-Crictoday' : 'CricToday' ?></title>
    <link rel="shortcut icon" href="/images/ctt.png">
    <link rel="canonical" href="<?php echo !empty($slug) ? $domain.$slug : $domain; ?>">
    <meta name="author" content="Crictoday"/>
    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta property="fb:app_id" content="358111494795002"/>
    <meta name="description" content="<?php echo !empty($metadesc) ? substr(strip_tags($metadesc), 0, 100).'..' : 'Crictoday is one of the world\'s leading sports website. Founded in 2010, Crictoday’s content includes news and features written by some of the world’s best writers.'; ?>"/>
    <meta name="keywords" content="<?php echo !empty($metakeywords) ? $metakeywords : 'News, Articles, Cricket,Sport'; ?>" />
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:title" content="<?php echo !empty($metatitle) ? $metatitle : 'Crictoday'; ?>"/>
    <meta name="twitter:url" content="<?php echo !empty($slug) ? $domain.$slug : $domain; ?>">
    <meta name="twitter:site" content="@crictoday">
    <meta name="twitter:description" content="<?php echo !empty($metadesc) ? substr(strip_tags($metadesc), 0, 100).'..' : 'Crictoday is one of the world\'s leading sports website. Founded in 2010, Crictoday’s content includes news and features written by some of the world’s best writers.'; ?>"/>
    <meta name="twitter:image" content="<?php echo !empty($metaimage) ? 'https://cms.crictoday.com/'.$metaimage : $domain.'crictoday.png'; ?>" />
  
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="article" />
    <meta property="og:title" content="<?php echo !empty($metatitle) ? $metatitle: 'Crictoday'; ?>" />
    <meta property="og:description" content="<?php echo !empty($metadesc) ? substr(strip_tags($metadesc), 0, 100).'..' : 'Crictoday is one of the world\'s leading sports website. Founded in 2010, Crictoday’s content includes news and features written by some of the world’s best writers.'; ?>" />
    <meta property="og:url" content="<?php echo !empty($slug) ? $domain.$slug : $domain; ?>" />
    <meta property="og:site_name" content="Crictoday" />
    <meta property="og:image:url" content="<?php echo !empty($metaimage) ? 'https://cms.crictoday.com/'.$metaimage : $domain.'crictoday.png'; ?>" />
    <meta property="og:image:secure_url" content="<?php echo !empty($metaimage) ? 'https://cms.crictoday.com/'.$metaimage : $domain.'crictoday.png'; ?>" />

    <meta name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1">
    <link rel="stylesheet" href="<?php echo $domain; ?>assets/css/normalize.css">
	<link rel="stylesheet" href="<?php echo $domain; ?>assets/css/slick.css">
	<link rel="stylesheet" href="<?php echo $domain; ?>assets/css/style.css">
	<link rel="stylesheet" href="<?php echo $domain; ?>assets/css/quiz.css">
	<script src="<?php echo $domain; ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo $domain; ?>assets/js/slick.js"></script>
	<script src="<?php echo $domain; ?>assets/js/5489632147892.js"></script>
	<script src="<?php echo $domain; ?>assets/js/axios.min.js"></script>
	<script src="<?php echo $domain; ?>assets/js/js.cookie.js"></script>
	<script src="<?php echo $domain; ?>assets/js/app.js"></script>
	<script src="<?php echo $domain; ?>controllers/header.js"></script>
	<script src="<?php echo $domain; ?>controllers/user.js"></script>
	<script src="<?php echo $domain; ?>controllers/cms.js"></script>
    <script type="application/ld+json">
      {
        "@context": "http://schema.org",
        "@type": "NewsArticle",
        "headline": "Open-source framework for publishing content",
        "datePublished": "2015-10-07T12:02:41Z",
        "image": [
          "logo.jpg"
        ]
      }
    </script>
    <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
    <script src="//platform-api.sharethis.com/js/sharethis.js#property=5b17c27674ac940011c1553a&product=inline-share-buttons"></script>
  </head>

  <body>
	<div>
		<div class="header">
		<div class="fl">
			<button class="menu-icon fl" id="ham-menu-icon"></button>
			<?php if(!isset($_COOKIE['lang']) || $_COOKIE['lang'] == "en") { ?>
				<a class="language-icon hindione" href="https://www.hindi.crictoday.com/" >Switch To Hindi</a>
		    <?php } else{ ?>
		    	<a class="language-icon englishone" href="https://www.crictoday.com/"><?php echo $language[0]->switchtoenglish; ?></a>
		    <?php } ?>
		</div>
		<a href="/"><span class="logo">Crictoday</span></a>
		<div class="fr">
		<a href="/write-for-us" class="writelink fl ng-binding">Write For Us</a>
		<a href="<?php echo $language[0]->facebooklink; ?>" target="_blank" class="facebook-icon fl">Facebook</a>
		<a href="<?php echo $language[0]->twitterlink; ?>" target="_blank" class="twitter-icon fl">Twitter</a>
		<a href="<?php echo $language[0]->instagramlink; ?>" target="_blank" class="insta-icon fl">Instagram</a>
		<?php
			if ($token == null) {
				echo '<!-- ngIf: !view_token --><div class="user-img fl ng-scope">
		<a href="javascript:void(0)" class="login-open">
			<amp-img src="'.$domain.'assets/images/userimage.png" srcset="'.$domain.'assets/images/userimage.png" width="38" height="38" layout="fixed" alt=""></amp-img>
		</a>
		</div><!-- end ngIf: !view_token -->';
			}
		if ($token != null) {
				echo '<div class="user-img fl"><a href="javascript:void(0)" class="user-navigation-open">';
					if ($username != null) {
						echo '<amp-img src="'.$domain.'assets/images/user-image.jpg" srcset="'.$domain.'assets/images/user-image.jpg" width="38" height="38" layout="fixed" alt=""></amp-img>';
					}/*else if($plusimage){
						echo '<img ng-if="plusimage || plusname || plusmail" src="{{plusimage}}" alt="" width="38" height="38">';
						else if($fimage){
						echo '<img ng-if="fimage || fmail || fname" src="{{fimage}}" alt="" width="38" height="38">';
					}*/
					echo '</a>
						<div class="user-navigation">
							<ul>
								<li><a href="javascript:void(0)" class="changepassword-open">'.$language[0]->changepassword.'</a></li>
								<li><a href="javascript:void(0)" onclick="logout()">'.$language[0]->logout.'</a></li>
							</ul>
						</div>	
					</div>';
			}	
		?>		
		
		<!-- ngIf: view_token -->

		<button class="search-icon fl"></button>
		<div class="clr"></div>
	</div>
		<div class="clr"></div>
		<div class="global-search-container"><input type="text" class="global-search" value="" placeholder="Search by keyword" id="srch" spellcheck="false" onkeyup="searchInput(event)" autofocus>

		
		<div class="result-search" id="result-search">
			
		</div>

		</div>
	</div>
	<div class="hamburger-menu" id="ham-menu">
		<div class="inside">
			<ul>
				<li><a href="<?php echo $domain; ?>"><?php echo $language[0]->home; ?></a></li>
				<?php 
				/* if(count($supercats) > 0){
					foreach ($supercats as $cats) {
						echo '<li class="'.$cats->sc_color_class.'"><a href="/category/'.$cats->sc_slug.'">'.$cats->sc_name.'</a>
							<ul class="sup_cats" style="">';
								foreach ($cats->sc_sub_category as $subcats) {
								echo '<li><a href="/category/'.$cats->sc_slug.'/'.$subcats->slug.'">'.$subcats->name.'</a></li>';
								}
							echo '<li ><a href="/category/'.$cats->sc_slug.'/photos">'.$language[0]->photos.'</a></li></ul>
							<span class="arrow" id="'.$cats->sc_slug.'"></span>	
							</li>';
					} 

				} */ ?>
				<?php 
				if(count($supercats) > 0){
					foreach ($supercats as $cats) {
						if($cats->sc_slug != 'f1' && $cats->sc_slug != 'tennis' && $cats->sc_slug != 'kabaddi' && $cats->sc_slug != 'others'){
							echo '<li class="'.$cats->sc_color_class.'"><a href="/category/'.$cats->sc_slug.'">'.$cats->sc_name.'</a>';
								if ($cats->sc_slug != 'wrestling' && $cats->sc_slug != 'football') {
									echo '<ul class="sup_cats" style="">';
									foreach ($cats->sc_sub_category as $subcats) {
										echo '<li><a href="/category/'.$cats->sc_slug.'/'.$subcats->slug.'">'.$subcats->name.'</a></li>';
										}
									echo '<li ><a href="/category/'.$cats->sc_slug.'/photos">'.$language[0]->photos.'</a></li></ul>';
									echo '<span class="arrow" id="'.$cats->sc_slug.'"></span>';	
								}

							
							echo '</li>';
						}
					} 

				}  ?>
				<li class="green"><a href="/photos" class="photo-icon"><?php echo $language[0]->photos; ?></a></li>
				<?php $x = 0; foreach ($trending as $trend) {
					if ($x == 0 || $x == 4) {
						echo '<li class="orange">';
						if ($trend->type == 'trending') {
							echo '<a href="/'.$trend->type.'s/">'.$trend->name.'</a>';
						}else{
							echo '<a href="/'.$trend->type.'/'.$trend->specials_slug.'">'.$trend->name.'</a>';
						}
						echo '</li>';
					}else if($x ==1){
						echo '<li class="drkgreen">';
					if ($trend->type == 'trending') {
						echo '<a href="/'.$trend->type.'s/">'.$trend->name.'</a>';
					}else{
						echo '<a href="/'.$trend->type.'/'.$trend->specials_slug.'">'.$trend->name.'</a>';
					}
					echo '</li>';
					}else if($x == 2){
						echo '<li class="drksblu">';
					if ($trend->type == 'trending') {
						echo '<a href="/'.$trend->type.'s/">'.$trend->name.'</a>';
					}else{
						echo '<a href="/'.$trend->type.'/'.$trend->specials_slug.'">'.$trend->name.'</a>';
					}
					echo '</li>';
					}
					$x++;
				}
				?>
				<!-- <li><a href="/ipl"><?php //echo $language[0]->ipl; ?></a></li> -->
				<li><a href="/teams"><?php echo $language[0]->teams; ?></a></li>
				<li><a href="/write-for-us"><?php echo $language[0]->writeforus; ?></a></li>
				<li><a href="/download-apps"><?php echo $language[0]->downloadapps; ?></a></li>
				<li><a href="/contact-us"><?php echo $language[0]->contactus; ?></a></li>
			</ul>
			<ul class="device-visible-only">
				<li><a href="/teams">Teams</a></li>
				<li><a href="/about-us">About Us</a></li>
				<li><a href="/feedback">Feedback</a></li>
				<li><a href="/career">Career</a></li>
				<li><a href="/privacy-policy">Privacy Policy</a></li>
				<li><a href="/terms-of-use">Terms Of Use</a></li>
			</ul>
		</div>
	</div>
	</div>
