<?php
function array2xml($array, $xml = false){

    if($xml === false){
        $xml = new SimpleXMLElement('<result/>');
    }

    foreach($array as $key => $value){
        if(is_array($value)){
            array2xml($value, $xml->addChild($key));
        } else {
            $xml->addChild($key, $value);
        }
    }

    return $xml->asXML();
}

$raw_data = file_get_contents('http://admincrictoday.grapesmobile.com/post-rss');
$jSON = json_decode($raw_data, true);

$xml = array2xml($jSON, false);

 header('Content-type: application/xml');
echo $xml;