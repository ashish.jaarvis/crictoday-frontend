<?php
include_once 'apis/apis.php';
$slug = 'privacy-policy';
$privacypolicy = new Apis('https://cms.crictoday.com/api/page-detail?locale='.$lang.'&page_slug=privacy-policy');
$privacypolicy = $privacypolicy->getData();

$metatitle = $privacypolicy[0]->name;
$metadesc = $privacypolicy[0]->content;
$metakeywords = $privacypolicy[0]->meta_keywords;

include_once 'header.php';
?>
<div>
	<div class="static-content">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="page-title"><?php echo $privacypolicy[0]->name; ?></h3>
					</div>
					<div class="col"><?php echo $privacypolicy[0]->content; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once 'footer.php'; ?>