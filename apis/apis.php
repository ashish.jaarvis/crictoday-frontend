<?php
ini_set('display_errors', 0);
class Apis{
	public $url;

	public function __construct($url = null){
		$this->url = $url;
	}

	public function getData(){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		$response = curl_exec($ch);
		if (curl_errno($ch)) { 
		   print curl_error($ch); 
		} 
		curl_close($ch);
		$response = json_decode($response);
		return $response;
	}
}
$cms = 'https://cms.crictoday.com/';
$domain = 'http://'.$_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/';
$lang = 'en';
if(!isset($_COOKIE['lang']) || $_COOKIE['lang'] == "en") {
$lang = 'en';
$language = new Apis($domain.'lang/en.json');

}else{
	$lang = 'hi';
	$language = new Apis($domain.'lang/hi.json');
}
$language = $language->getData();
$slider = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&category_id=14&&type=slider');
$hometoppost4 = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=hometoppost4');
$homevideo = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=videosall&limit=9');
$homelisting = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=homelisting&limit=10&offset=0');
$trending = new Apis('https://cms.crictoday.com/api/specials-list?locale='.$lang);
$teamranking = new Apis('https://cms.crictoday.com/api/teams-ranking?locale='.$lang);
$supercats = new Apis('https://cms.crictoday.com/api/super-categories?locale='.$lang);

$homeslider = $slider->getData();
$hometop4post = $hometoppost4->getData();
$homevideo = $homevideo->getData();
$homelisting = $homelisting->getData();
$trending = $trending->getData();
$teamranking = $teamranking->getData();
$supercats = $supercats->getData();
?>