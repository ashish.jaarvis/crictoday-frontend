<?php
include_once 'apis/apis.php';
$slug = 'write-for-us';
$metatitle = 'Write for us';
include_once 'header.php';
?>
<div class="inner-page">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col margin-botton40">
					<h3 class="page-title center"><?php echo $language[0]->uploadyourarticle; ?></h3>
				</div>

				<form name="writeform" id="writeform" autocomplete="off" novalidate>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->firstname; ?><span>*</span></label>
							<input type="text"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="firstname" id="firstname">
						</div>
					</div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->lastname; ?><span>*</span></label>
							<input type="text"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="lastname" id="lastname">
						</div>
					</div>
					<div class="clr"></div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->email; ?><span>*</span></label>
							<input type="email"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="email" id="email">
						</div>
					</div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->mobilenumber; ?><span>*</span></label>
							<input type="text"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="mobile" id="mobile" maxlength="10" minlength="10">
						</div>
					</div>
					<div class="clr"></div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->uploadyourarticle; ?><span>*</span></label>
							<div class="customfile"><input class="uploadfield" type="file" name="comming_image" id="comming_image" base-sixty-four-input required onload="onLoad" maxsize="1000000" accept=".docx,.pdf,.png">
							<label for="comming_image" class="uploadlabel"></label>
							<div id="error"></div>
						</div>
						</div>
					</div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->pastevideourl; ?><span class="no-required">(<?php echo $language[0]->optional; ?>)</span></label>
							<input type="text" class="textbox" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" id="video_url" name="video_url">
						</div>
					</div>
					<div class="clr"></div>
					<div class="col">
						<div class="form-row">
							<p>All information, text, sound, photographs, graphics, video or other materials("content"), whether publicly or privately transmitted / posted, is the sole responsibility of the person from where such content is originated. Editorial contributions via this portal is non-commercial and will only be used if our editors find it relevant for our readers. You must be over 18 years of age, and you and your submissions will be governed by our terms and conditions. Please read them carefully before using the site.</p>
						</div>
					</div>
					<div class="col">
						<div class="form-row">
							<input type="checkbox" id="accept" name="accept"	 required="">
							<label for="accept"><?php echo $language[0]->iaccept; ?><span>*</span></label>
						</div>
					</div>
					<div id="showArticalErrors" class="form-row"></div>
					<div class="col">
						<div class="form-row">
							<button class="button-w" type="button" onclick="return writefun()"><span><?php echo $language[0]->submit; ?></span></button>
						</div>
					</div>
					<div class="clr"></div>
				</form>	
			</div>
		</div>
	</div>
</div>
<div class="form-loader"></div>
<script type="text/javascript">
	writefun = function(){
		$('.form-error').remove();
		$('#error').empty();
		$('#writeform button').attr('disabled', 'disabled');
	   	$(".form-loader").show();
	   	var formData = new FormData();
		var imagefile = document.querySelector('#comming_image');
		if(!imagefile.files[0]) {
			$('#error').html('<p class="mr form-error" >Please upload a file.</p>');
			$(".form-loader").hide();
			$('#writeform button').removeAttr('disabled');
			return false;
		} else if(eval(imagefile.files[0].size) > 1000000){
			$('#error').html('<p class="mr form-error" >Please upload file less than 500kb.</p>');
			$(".form-loader").hide();
			$('#writeform button').removeAttr('disabled');
			return false;
		}
		if (!$('#accept').prop('checked')) {
			$('#accept').after('<p class="mr form-error" >Please accept all term & conditions.</p>');
			$(".form-loader").hide();
			$('#writeform button').removeAttr('disabled');
			return false;
		}
		formData.append("comming_image", imagefile.files[0]);
		var obj = $('#writeform').serializeObject();
		$.each(obj, function(k, v){
			formData.append(k, v);
		});
	   		axios.post(siteurl+'article-submission',formData, { 
			 headers: { 'Content-Type': 'multipart/form-data' }
			})
			.then(function(){
				$('#showArticalErrors').empty();
				$('#error').empty();
				$('.form-error').remove();
	   				setTimeout(function() {
					$(".form-loader").hide();	
					$('form')[0].reset();
					$('#writeform button').removeAttr('disabled');
					$(".popup-thank.submitapplication").show();
				},4000);
			})
			.catch(error => {
			$(".form-loader").hide();
     		var errorsHtml="";
     		var err = error.response.data;
     		$('.form-error').remove();
     		if(err.status_code === "0" ){	
				if (err.errors) {
				$.each(err.errors, (k, v) => {
					$('#'+k).after('<p class="mr form-error" >'+v[0]+'</p>');
				});
				}else{
					errorsHtml += '<p class="mr form-error" >'+err.message+'</p>';
				}
			}	
			$('#writeform button').removeAttr('disabled');
     		$('#showArticalErrors').html(errorsHtml);
			});
	   }
</script>
<?php include_once 'footer.php'; ?>