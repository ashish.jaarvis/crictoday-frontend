$(document).ready(function(){
	$(document).on('click',".menu-icon",function() {
		$(this).toggleClass("up");
		$(".hamburger-menu").toggleClass("active");
		$("body").toggleClass("fixed");
	})
	$(document).on('click',".hamburger-menu li a",function() {
		$(".menu-icon").removeClass("up");
		$(".hamburger-menu").removeClass("active");
		$("body").removeClass("fixed");
	})
	$(document).on('click',".ranking-top-tab a",function() {
		var get_id = $(this).attr("id");
		$(".ranking-content").hide();
		$("body").find(".content-"+ get_id).show();
		$(".ranking-top-tab a").removeClass("active");
		$(this).addClass("active");
	})
	$(document).on('click',".result-tab a",function() {
		var get_id = $(this).attr("id");
		$(this).parent().parent().find(".result-container").hide();
		$(this).parent().parent().find(".content-"+ get_id).show();
		$(this).parent().find("a").removeClass("active");
		$(this).addClass("active");
	})
	$(document).on('click',".tap-link li a",function() {
		var get_id = $(this).attr("id");
		$(this).parent().parent().parent().parent().find(".pop-content").hide();
		$(this).parent().parent().parent().parent().find(".content-"+ get_id).show();
		$(this).parent().parent().find("li").removeClass("active");
		$(this).parent().addClass("active");
	})
	$(document).on('click',".player-performance-tab a",function() {
		var get_id = $(this).attr("id");
		$(this).parent().parent().find(".per-5-content").hide();
		$(this).parent().parent().find(".content-per-"+ get_id).show();
		$(this).parent().parent().find(".player-performance-tab a").removeClass("active");
		$(this).addClass("active");
	})
	$(document).on('click',".selected-team",function() {
		$(this).toggleClass("active");
		$(".teams-list").toggle();
	})



	$(document).on('click',".login-open",function() {
		$("body").addClass("fixed");
		$(".popup-register").hide();
		$(".popup-login").show();
	})
	$(document).on('click',".register-open",function() {
		$("body").addClass("fixed");
		$(".popup-login").hide();
		$(".popup-register").show();
	})
	$(document).on('click',".forgot-open",function() {
		$("body").addClass("fixed");
		$(".popup-login").hide();
		$(".popup-forgot").show();
	})
	$(document).on('click',".changepassword-open",function() {
		$("body").addClass("fixed");
		$(".popup-base").hide();
		$(".popup-changepassword").show();
	})
	$(document).on('click',".popup-base .close",function() {
		$("body").removeClass("fixed");
		$(".popup-base").hide();
	})
	$(document).on('click',".close-email-error",function() {
		$(".popup-email-error").hide();
		$(".popup-register").show();
	})
	 $(document).on('change', "input[type='file']", function() {
	  var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
	  $(this).parent().find("label").text(filename);
	 });


	 $(document).on('click',".hamburger-menu li span.arrow",function() {
	 	 
          $('.hamburger-menu li span.arrow').removeClass('minsing');
         
	 	if($(this).parent().find("ul").is(':visible')){
	 		$(this).parent().find("ul").slideToggle();
	 		$(this).addClass('minsing');
			$(this).removeClass('minsing');
	 	}
	 	else{
       
          $(this).addClass('minsing');
           
			$(".hamburger-menu li ul").slideUp();
		
			$(this).parent().find("ul").slideDown();

	 	}
	 	
		
	})
	 $(document).on('click', ".user-navigation-open", function() {
	  	$(".user-navigation").toggle();
	  	$(".global-search-container").hide();
	 });

	$(document).on('click', ".opn-p-des", function() {
		var get_img = $(this).find("img").attr("src");
		var get_n = $(this).parent().parent().find(".p-info.name").text();
		var get_d = $(this).parent().parent().find("input.data").val();
		$(".popup-base.profile-p .content").find("img").attr("src",get_img)
		$(".popup-base.profile-p .content").find("h4").text(get_n)
		$(".popup-base.profile-p .content .data").html(get_d)
		$("body").addClass("fixed");
		$(".popup-base").hide();
	  	$(".popup-base.profile-p").show();
	 }); 


	$(document).on('click', "a.full-team-statistics", function() {
		$("body").addClass("fixed");
		$(".popup-base.performence-p").show();
	 }); 
	$(document).on('click', "button.search-icon", function() {
		$(".global-search-container").toggle();
		$(".user-navigation").hide();
	 });
	 $(document).on('click', ".repeat-search a", function() {
		$(".global-search-container").hide();
	 }); 


/*

  $('.prev').on('click', function() {
      var i = $(".active").index();
          i--;
      $(".active").removeClass('active');
      $('.slides-show li').eq(i).addClass('active');
      $('html, body').animate({
        scrollTop:$('.slides-show li').eq(i).offset().top -70
      }, 600);
      return false;
  });

  $('.next').on('click', function() {
      var i = $(".active").index();
          i = i >= $('.slides-show li').length-1 ? 0 : i+1;
      $(".active").removeClass('active');
      $('.slides-show li').eq(i).addClass('active');
      $('html, body').animate({
        scrollTop:$('.slides-show li').eq(i).offset().top -70
      }, 600);
      return false;
  });*/




 $(window).scroll(function() {
    var scroll = $(window).scrollTop();

    if (scroll >= 160) {
        $(".photo-detail-page").addClass("picarea");
    } else {
        $(".photo-detail-page").removeClass("picarea");
    }
});











	
});