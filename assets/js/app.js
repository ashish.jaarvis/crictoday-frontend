'use strict';
var siteurl = "https://cms.crictoday.com/api/";
var cmsurl = "https://cms.crictoday.com/";
var endurl = "?locale=";
var langurl = "http://101.53.139.221:82/lang/";
var site_base_url = "http://101.53.139.221:82/";

var lang_custom = localStorage.getItem("lang");
var splited = window.location.pathname.split("-");

if (window.location.pathname.includes("detail")){
	if ( splited[splited.length - 1] == "hi"){
		lang_custom = "hi"
	}
	else{
		lang_custom = "en"
	}
	localStorage.setItem("lang",lang_custom)
	
}
$.fn.serializeObject = function()
{
   var o = {};
   var a = this.serializeArray();
   $.each(a, function() {
       if (o[this.name]) {
           if (!o[this.name].push) {
               o[this.name] = [o[this.name]];
           }
           o[this.name].push(this.value || '');
       } else {
           o[this.name] = this.value || '';
       }
   });
   return o;
};

function teamfilter(id){
  $('.teamsfilter').hide();
  $('#'+id).show();
}

window.fbAsyncInit = function() {
    FB.init({
      appId            : '358111494795002',
      autoLogAppEvents : true,
      xfbml            : true,
      version          : 'v2.10',
      status           : true
    });

    FB.getLoginStatus(function(response){
      if(response.status === 'connected'){
        console.log('response data');
        console.log(response);
      } else if (response.status === 'not_authorized'){
        // not auth
      } else {
        // we are not logged in to facebook
      }
    })


  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));