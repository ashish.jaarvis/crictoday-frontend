<div>
<div class="footer">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="download-text fl">
						<!-- <span class="mobile-icon"></span>
						<?php //echo $language[0]->downloadourmobileapp; ?><span><?php //echo $language[0]->itseasyandfast; ?></span>

						<div class="download-icon fl">
						<a href="#" class="android"></a>
						<a href="#" class="ios"></a>
					</div> -->
					</div>

				
					

			<div class="newssubscribess col-30 fr">
				<h2>Sign Up for Our Newsletter</h2>
			      <form name="subsform" id="subsform" autocomplete="off" novalidate>
			            <div class="field newsletter">
			              
			                <div class="control">
			                   <input type="email"  autocomplete="off"  spellcheck="false" class="textbox" required="" name="email" id="email" placeholder="Please enter your email id">
			                   <div id="showSubsErrors"></div>
			                     <!-- <input type="submit" value="SubScribe" onclick="subscribefun()"  class="sumt" ng-disabled="subsform.$invalid"> -->

			                     <input type="button" value="SubScribe" onclick="subscribefun()" class="sumt">
			                </div>


			            </div>
			            
			        </form>

			    </div>
			    	<div class="download-pdf fl <?php echo $language[0]->downloadmagazine_file; ?>">
						<a href="/assets/downloadmagazine/<?php echo $language[0]->downloadmagazine_file; ?>.pdf" target="_blank" class="bbtn" download > <?php echo $language[0]->downloadmagazine_text; ?></a>
  

<div class="footer-social">
  <a href="https://www.facebook.com/crictodayindia/" target="_blank" class="facebook-icon fl">Facebook</a>
		<a href="https://twitter.com/crictoday" target="_blank" class="twitter-icon fl">Twitter</a>
		<a href="https://www.instagram.com/crictoday/?hl=en" target="_blank" class="insta-icon fl">Instagram</a>


	</div>
					</div>

					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="footer-base">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer-nav fl">
						<ul>
							<li><a href="/teams"><?php echo $language[0]->teams; ?></a></li>
							<li><a href="/about-us"><?php echo $language[0]->aboutus; ?></a></li>
							<li><a href="/feedback">Feedback</a></li>
							<li><a href="/career"><?php echo $language[0]->career; ?></a></li>
							<li><a href="/privacy-policy">Privacy Policy</a></li>
							<li><a href="/terms-of-use">Terms Of Use</a></li>
							<li><a href="/feed.xml" target="_blank">RSS Feed</a></li>

						</ul>
					</div>
					<div class="copyright fr"></div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>



<div class="popup-base popup-login">
	<div class="in">

		<div class="close"></div>


	<div class="form-row">
	<button type="button" class="facebooklogin" onclick="fbloginfunc()"><span><?php echo $language[0]->loginwithfacebook; ?></span></button>
	</div>


	<div class="form-row">
	<button class="googlelogin" onclick="googleloginfunc()"><span><?php echo $language[0]->loginwithgoogleplus; ?></span></button> 
	</div>
	
	<div class="form-or-seprator"><span><?php echo $language[0]->or; ?></span></div> 




		<form name="loginform" id="loginform" autocomplete="off" novalidate>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->emailid; ?></label>
				<input type="email"  autocomplete="off"  spellcheck="false" class="textbox" required="" name="email">
			</div>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->password; ?></label>
				<input type="password"  autocomplete="off"  spellcheck="false" class="textbox" required="" name="password">
			</div>
			<div class="form-row" id="showErrors"></div>
			<div class="form-row">
				<button class="button-w" type="button" onclick="loginfun()"><?php echo $language[0]->submit; ?></button>
				<a href="javascript:void(0)" class="fr forgot-password-lnk forgot-open"><?php echo $language[0]->forgotpassword; ?></a>
			</div>
		</form>
		<div class="form-row">
			<?php echo $language[0]->donothaveaccountwithus; ?> <a href="javascript:void(0)" class="register-lnk register-open"><?php echo $language[0]->registernow; ?></a>
		</div>		
		<div class="popup-loader"></div>



		

	</div>
</div>






<div class="popup-base popup-register">
	<div class="in">
		<div class="close"></div>
		<form name="registrationform" id="registrationform" autocomplete="off" novalidate>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->name; ?></label>
				<input type="text"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="name" id="name">
			</div>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->emailid; ?></label>
				<input type="email"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="email">		
			</div>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->mobilenumber; ?></label>
				<input type="text"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="mobile" id="mobile" maxlength="10" minlength="10">
			</div>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->password; ?></label>
				<input type="password"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="password">
			</div>
			<div class="form-row" id="showRegErrors"></div>
			<div class="form-row">
				<button class="button-w" type="button" onclick="registerfun()"><?php echo $language[0]->submit; ?></button>
			</div>
		</form>
		<div class="form-row">
			<?php echo $language[0]->alreadyhaveaccountwithus; ?> <a href="javascript:void(0)" class="register-lnk login-open"><?php echo $language[0]->alreadylogin; ?></a>
		</div>
		<div class="popup-loader"></div>
	</div>
</div>


<div class="popup-base popup-forgot">
	<div class="in">
		<div class="close"></div>
		<form name="forgetform" id="forgetform" autocomplete="off" novalidate>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->emailid; ?></label>
				<input type="email"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="email">
			</div>
			<div class="form-row" id="showForgetErrors"></div>
			<div class="form-row">
				<button class="button-w" type="button" onclick="forgetfun()"><?php echo $language[0]->submit; ?></button>
			</div>
		</form>
		<div class="popup-loader"></div>
	</div>
</div>


<div class="popup-base popup-changepassword">
	<div class="in">
		<div class="close"></div>
		<form name="changepassword" id="changepasswordform" autocomplete="off" novalidate>
			<div class="form-row">
				<input type="hidden" name="token" value="<?php echo $_COOKIE['token']; ?>">
				<label for="" class="labs"><?php echo $language[0]->currentpassword; ?></label>
				<input type="password"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="cpassword" id="cpassword">
			</div>
			<div class="form-row">
				<label for="" class="labs"><?php echo $language[0]->newpassword; ?></label>
				<input type="password"  autocomplete="off"   spellcheck="false" class="textbox" required="" name="password" id="password">
			</div>
			<div class="form-row" id="showChangePasswordErrors"></div>
			<div class="form-row">
				<button class="button-w" type="button" onclick="changepasswordfun()"><?php echo $language[0]->submit; ?></button>
			</div>
		</form>
		<div class="popup-loader"></div>
	</div>
</div>


<div class="popup-base popup-thank registerthanks">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Thank You</div>
		<div class="content-pop"><p>for registring with us. Please check your email for more info.</p></div>
	</div>
</div>
<div class="popup-base popup-thank forgetthanks">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Password Emailed</div>
		<div class="content-pop"><p>We have send your password to your registered email address. <br>check your email.</p></div>
	</div>
</div>
<div class="popup-base popup-thank changepassword">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Password Changed</div>
		<div class="content-pop"><p>Your password has been changed successfully.</p></div>
	</div>
</div>
<div class="popup-base popup-thank wtitesubmit">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Article Submitted</div>
		<div class="content-pop"><p>We will review this article and give you update soon.</p></div>
	</div>
</div>
<div class="popup-base popup-thank fedbacksubmit">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Feedback Submitted</div>
		<div class="content-pop"><p>We will review this and give you update soon.</p></div>
	</div>
</div>
<div class="popup-base popup-thank subscribesubmit">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Subscribed</div>
		<div class="content-pop"><p>Thank you for subscribing for our daily updates</p></div>
	</div>
</div>
<div class="popup-base popup-thank commentsubmit">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Thank You</div>
		<div class="content-pop"><p>We will review your comment and post.</p></div>
	</div>
</div>
<div class="popup-base popup-thank contactthanks">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Thank You</div>
		<div class="content-pop"><p>for contacting us. Our representative will get back to you shortly.</p></div>
	</div>
</div>

<div class="popup-base popup-email-error">
	<div class="in">
		<div class="close-email-error"></div>
		<div class="error-icon"></div>
		<div class="alert-message">Oops!</div>
		<div class="content-pop"><p>This email id is already registered with us.<br>Try Diffrent Email Id</p></div>
	</div>
</div>

<div class="popup-base popup-thank submitapplication">
	<div class="in">
		<div class="close"></div>
		<div class="thank-icon"></div>
		<div class="alert-message">Thank You</div>
		<div class="content-pop popup-message-text"><p>for showing interest in working with us.</p></div>
	</div>
</div>