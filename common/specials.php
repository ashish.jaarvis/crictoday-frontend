<?php
$iccrankings = new Apis('https://cms.crictoday.com/api/teams-ranking?locale=en');
$iccranking = $iccrankings->getData();

?>
<div class="col-30 fl">
<div class="widget ng-scope"><div class="widget-title ng-binding ng-scope"><?php echo $language[0]->iccrankings; ?></div>
<div class="ranking-top-tab ng-scope">
	<a href="javascript:void(0)" class="active ng-binding" id="test"><?php echo $language[0]->test; ?></a>
	<a href="javascript:void(0)" id="odi" class="ng-binding"><?php echo $language[0]->odi; ?></a>
	<a href="javascript:void(0)" id="t20" class="ng-binding"><?php echo $language[0]->t20; ?></a>
	<div class="clr"></div>
</div>
<div class="ranking-content content-test ng-scope">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tbody><tr>
			<th class="ng-binding"><?php echo $language[0]->pos; ?></th>
			<th class="ng-binding"><?php echo $language[0]->team; ?></th>
			<th class="ng-binding"><?php echo $language[0]->points; ?></th>
			<th class="ng-binding"><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php
			$testteam = array_filter($iccranking,function($type){
				return $type->team_type === 'TEST';
			});
			foreach ($testteam as $team) {
				echo '<tr class="ng-scope">
						<td valign="middle" class="ng-binding">'.$team->sorting.'</td>
						<td valign="middle"><img src="https://cms.crictoday.com/'.$team->team_flag_image.'" width="26" height="26" style="float:left; position:relative; top:1px;"><span style="float:left; display:inline-block; line-height:26px;" class="ng-binding">'.$team->team_name.'</span></td>
						<td valign="middle" class="ng-binding">'.$team->points.'</td>
						<td valign="middle" class="ng-binding">'.$team->ratings.'</td>
					</tr>';
			}
		?>
	</tbody></table>
</div>
<div class="ranking-content content-odi ng-scope">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tbody><tr>
			<th class="ng-binding"><?php echo $language[0]->pos; ?></th>
			<th class="ng-binding"><?php echo $language[0]->team; ?></th>
			<th class="ng-binding"><?php echo $language[0]->points; ?></th>
			<th class="ng-binding"><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php
			$oditeam = array_filter($iccranking,function($type){
				return $type->team_type === 'ODI';
			});
			foreach ($oditeam as $team) {
				echo '<tr class="ng-scope">
						<td valign="middle" class="ng-binding">'.$team->sorting.'</td>
						<td valign="middle"><img src="https://cms.crictoday.com/'.$team->team_flag_image.'" width="26" height="26" style="float:left; position:relative; top:1px;"><span style="float:left; display:inline-block; line-height:26px;" class="ng-binding">'.$team->team_name.'</span></td>
						<td valign="middle" class="ng-binding">'.$team->points.'</td>
						<td valign="middle" class="ng-binding">'.$team->ratings.'</td>
					</tr>';
			}
		?>
	</tbody></table>
</div>
<div class="ranking-content content-t20 ng-scope">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tbody><tr>
			<th class="ng-binding"><?php echo $language[0]->pos; ?></th>
			<th class="ng-binding"><?php echo $language[0]->team; ?></th>
			<th class="ng-binding"><?php echo $language[0]->points; ?></th>
			<th class="ng-binding"><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php
			$t20 = array_filter($iccranking,function($type){
				return $type->team_type === 'T20';
			});
			foreach ($t20 as $team) {
				echo '<tr class="ng-scope">
						<td valign="middle" class="ng-binding">'.$team->sorting.'</td>
						<td valign="middle"><img src="https://cms.crictoday.com/'.$team->team_flag_image.'" width="26" height="26" style="float:left; position:relative; top:1px;"><span style="float:left; display:inline-block; line-height:26px;" class="ng-binding">'.$team->team_name.'</span></td>
						<td valign="middle" class="ng-binding">'.$team->points.'</td>
						<td valign="middle" class="ng-binding">'.$team->ratings.'</td>
					</tr>';
			}
		?>
	</tbody></table>
</div></div>
<div class="widget ng-scope">
	<?php
			$serieslist = array_filter($trending, function($type){
				return $type->type === 'series';
			});
			if(count($serieslist)){
			?>
				<div class="series-rep ng-scope">
					<div class="white-bg">
						<div class="pinn-title">
							<a href="series/ind-vs-nz" class="ng-binding ng-scope"><?php echo $serieslist[0]->name ?></a></div>
			<?php foreach ($serieslist[0]->posts as $series)
			{
			    echo '<div class="series-listing ng-scope">
							<a href="/detail/'.$series->content_slug.'" class="ng-binding">'.$series->title.'</a>
						</div>';
			}?>
			</div>
		</div>
	<?php
	}
		?>	
	<!-- end ngIf: data.type!='ipl' --><!-- end ngRepeat: data in specials --><!-- ngIf: data.type!='ipl' --><div class="series-rep ng-scope">
	<div class="white-bg">
		<?php
			$trends = array_filter($trending, function($type){
				return $type->type === 'trending';
			});
			if (count($trends) > 0) {
				echo '<div class="pinn-title">
							<a href="/trending" class="ng-binding ng-scope">'.$trends[0]->name.'</a>
						</div>';
				foreach ($trends[0]->posts as $trend)
				{
				    echo '<div class="series-listing ng-scope">
								<a href="/detail/'.$trend->content_slug.'" class="ng-binding">'.$trend->title.'</a>
							</div>';
				}
			}
		?>
	</div>
</div></div>

					<a class="playthequiz" href="/quiz">Play The Quiz</a>

				</div>