<?php 
	$iccrankings = new Apis('https://cms.crictoday.com/api/teams-ranking?locale=en');
	$iccranking = $iccrankings->getData();

?>
<div class="widget-title"><?php echo $language[0]->iccrankings; ?></div>
<div class="ranking-top-tab">
	<a href="javascript:void(0)" class="active" id="test"><?php echo $language[0]->test; ?></a>
	<a href="javascript:void(0)" id="odi"><?php echo $language[0]->odi; ?></a>
	<a href="javascript:void(0)" id="t20"><?php echo $language[0]->t20; ?></a>
	<div class="clr"></div>
</div>
<div class="ranking-content content-test">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tr>
			<th><?php echo $language[0]->pos; ?></th>
			<th><?php echo $language[0]->team; ?></th>
			<th><?php echo $language[0]->points; ?></th>
			<th><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php 
			foreach ($iccranking as $testeam) {
				if ($testeam->team_type == 'TEST') {
					echo '<tr>
								<td valign="middle">'.$testeam->sorting.'</td>
								<td valign="middle"><img src="'.$cms.$testeam->team_flag_image.'" width="26" height="26" layout="fixed" style="float:left; position:relative; top:1px;"></img><span class="rtname" style="float:left; display:inline-block; line-height:26px;">'.$testeam->player_name.'</span></td>
								<td valign="middle">'.$testeam->points.'</td>
								<td valign="middle">'.$testeam->ratings.'</td>
							</tr>';
				}
			}
		?>
	</table>
</div>
<div class="ranking-content content-odi">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tr>
			<th><?php echo $language[0]->pos; ?></th>
			<th><?php echo $language[0]->team; ?></th>
			<th><?php echo $language[0]->points; ?></th>
			<th><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php 
			foreach ($iccranking as $testeam) {
				if ($testeam->team_type == 'ODI') {
					echo '<tr>
								<td valign="middle">'.$testeam->sorting.'</td>
								<td valign="middle"><img src="'.$cms.$testeam->team_flag_image.'" width="26" height="26" layout="fixed" style="float:left; position:relative; top:1px;"></img><span class="rtname" style="float:left; display:inline-block; line-height:26px;">'.$testeam->player_name.'</span></td>
								<td valign="middle">'.$testeam->points.'</td>
								<td valign="middle">'.$testeam->ratings.'</td>
							</tr>';
				}
			}
		?>
	</table>
</div>
<div class="ranking-content content-t20">
	<table border="0" cellspacing="0" cellpadding="0" class="rank-table">
		<tr>
			<th><?php echo $language[0]->pos; ?></th>
			<th><?php echo $language[0]->team; ?></th>
			<th><?php echo $language[0]->points; ?></th>
			<th><?php echo $language[0]->rating; ?></th>
			
		</tr>
		<?php 
			foreach ($iccranking as $testeam) {
				if ($testeam->team_type == 'T20') {
					echo '<tr>
								<td valign="middle">'.$testeam->sorting.'</td>
								<td valign="middle"><img src="'.$cms.$testeam->team_flag_image.'" width="26" height="26" layout="fixed" style="float:left; position:relative; top:1px;"></img><span class="rtname" style="float:left; display:inline-block; line-height:26px;">'.$testeam->player_name.'</span></td>
								<td valign="middle">'.$testeam->points.'</td>
								<td valign="middle">'.$testeam->ratings.'</td>
							</tr>';
				}
			}
		?>
	</table>
</div>