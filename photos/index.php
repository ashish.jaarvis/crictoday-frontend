<?php
include_once '../apis/apis.php';
$photos = new Apis('https://cms.crictoday.com/api/photos-list?locale='.$lang);
$photos = $photos->getData();
$slug = 'photos';

include_once '../header.php';
?>
<div class="photos-main-page">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col-30 fr">
				<?php
					if(is_array($photos) && count($photos) > 0) {

						foreach ($photos as $key => $photo) {
							if ($key < 1) {
								echo '<div class="latest-pinn device-show"">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="caption">'.$photo->main_photo_caption.'</span>
									</a>
									<span class="counts">'.$photo->photos_total.'</span>
									<div class="dets">
										<div class="cat-title fl">'.$language[0]->photos.'</div>
										<div class="cat-tag fr">'.$photo->super_category.'</div>
										<div class="clr"></div>
										<div class="name">'.$photo->title.'</div>
										<div class="date">'.$photo->created_at.'</div>
									</div>
								</div>';
							}else{ break; }
						}
						foreach ( array_slice($photos, 1) as $key => $photo) {
							if ($key < 10) {
								echo '<div class="latest-photo-block">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="counts">'.$photo->photos_total.'</span>
										<div class="pos">
											<div class="tag"><span class="sky">'.$photo->super_category.'</span></div>
											<div class="title-photo">'.$photo->title.'</div>
											<div class="date-photo">'.$photo->created_at.'</div>
										</div>
									</a>
								</div>';
							} else { break; }
						}
					}else{
						echo '<div class="white-bg comm-box">No Record Found.</div>';
					}
					?>
				</div>
				<div class="col-70 fl">
					<?php 
				if(is_array($photos) && count($photos) > 0) {

					foreach ($photos as $key => $photo) {
						if ($key < 1) {
							echo '<div class="latest-pinn device-none">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="caption">'.$photo->main_photo_caption.'</span>
									</a>
									<span class="counts">'.$photo->photos_total.'</span>
									<div class="dets">
										<div class="cat-title fl">'.$language[0]->photos.'</div>
										<div class="cat-tag fr">'.$photo->super_category.'</div>
										<div class="clr"></div>
										<div class="name">'.$photo->title.'</div>
										<div class="date">'.$photo->created_at.'</div>
									</div>
								</div>';
						}else{ break; }
					}

					foreach (array_slice($photos, 1, 15) as $key => $photo) {
						
							echo '<div class="listed-news">
									<div class="white-bg">
										<div class="common-state sky-stage no-hover">						
											<div class="news-inner">
											<span class="counts">'.$photo->photos_total.'</span>
											<amp-img src="'.$cms.$photo->main_photo.'" alt="'.$photo->super_category.'" width="285" height="161" layout="fixed"></amp-img>
												<div class="device-left">
												<div class="cat-title fl">'.$language[0]->photos.'</div>
												<div class="cat-tag fr">'.$photo->super_category.'</div>
												<div class="clr"></div>
												<h4><a href="/photos/'.$photo->photo_slug.'">'.$photo->title.'</a></h4>
												<div class="author-caption no-break">'.$photo->author.'<span>'.$photo->created_at.'</span></div>
												</div>
												<div class="clr"></div>
											</div>	
										
										</div>
									</div>
								</div>';
						
					}
				}else{
						echo '<div class="white-bg comm-box">No Record Found.</div>';
					}
				?>				
				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</div>
<?php include_once '../footer.php'; ?>