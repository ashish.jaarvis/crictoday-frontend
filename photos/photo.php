<?php 
include_once '../apis/apis.php';
$slug = $_REQUEST['photo_slug'];
if (!isset($slug)) {
	exit('Page not found!!');
}
$photos = new Apis('https://cms.crictoday.com/api/photos-detail?locale='.$lang.'&photo_slug='.$slug);
$photos = $photos->getData();

$slug = 'photos/'.$slug;
$metatitle = $photos[0]->title;

include_once '../header.php';
?>
<div class="search-main phtsrch">
  <div class="ads-space">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="main-title photitle"><?php echo $photos[0]->title; ?></div>
        </div>
        <div class="clr"></div>
      </div>
    </div>
  </div>
</div>


<div class="photo-detail-page">
  <div class="ads-space">
    <div class="container">
      <div class="row">
        <div class="galleryarea">
          <div class="col-70 fl">
            <div class="series-banner margin-botton20 allpiclist" >
              <div class="pagbutton">
              	<button class="prev">Prev</button>
              <button class="next">Next</button>


          </div>
              <ul  class="slides-show">
               
                 <li class="pics-list-block selected"> <img src="<?php echo $cms.$photos[0]->main_photo; ?>" alt="<?php echo $photos[0]->main_photo_caption; ?>" class="full">
                  <div class="space">
                    <div class="title"><?php echo $photos[0]->main_photo_caption; ?></div>
                  </div>
                </li>
                
                <?php 
                	foreach ($photos[0]->photos_gallery as $key => $photo) {
                		echo '<li class="pics-list-block"> <img src="'.$cms.$photo->sub_photo.'" alt="'.$photo->sub_photo_caption.'" class="full">
			                  <div class="space">
			                    <div class="title">'.$photo->sub_photo_caption.'</div>
			                  </div>
			                </li>';
                	}
                ?>                
              </ul>
              
       
              
            </div>
          </div>
           <div class="col-30    fl">
            <div class="gallery-tag">
              <h2>Related Galleries</h2>
              <ul>
                <?php 
                	foreach ($photos[0]->relative_list as $key => $photo) {
                		echo '<li>
				                <a href="/photos/'.$photo->photo_slug.'">
				                  <img src="'.$cms.$photo->main_photo.'" alt="'.$photo->title.'" />
				                  <p>'.$photo->title.'</p>
				                </a>
				              </li>';
                	}
                ?>              
              </ul>
              <h2>From Around The Web</h2>
              <ul>
                <?php 
                	foreach ($photos[0]->othercat_list as $key => $photo) {
                		echo '<li>
				                <a href="/photos/'.$photo->photo_slug.'">
				                  <img src="'.$cms.$photo->main_photo.'" alt="'.$photo->title.'" />
				                  <p>'.$photo->title.'</p>
				                </a>
				              </li>';
                	}
                ?> 
              </ul>
              <div class="clr"></div>
            </div>
          </div>
          <div class="clr"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	
$(document).ready(function(){

	 $('.prev').on('click', function() {
      var i = $(".active").index();
          i--;
      $(".active").removeClass('active');
      $('.slides-show li').eq(i).addClass('active');
      $('html, body').animate({
        scrollTop:$('.slides-show li').eq(i).offset().top -70
      }, 600);
      return false;
  });

  $('.next').on('click', function() {
      var i = $(".active").index();
          i = i >= $('.slides-show li').length-1 ? 0 : i+1;
      $(".active").removeClass('active');
      $('.slides-show li').eq(i).addClass('active');
      $('html, body').animate({
        scrollTop:$('.slides-show li').eq(i).offset().top -70
      }, 600);
      return false;
  });


    });

</script>

<?php
include_once '../footer.php';
?>