<?php
include_once 'header.php';
$privacypolicy = new Apis('https://cms.crictoday.com/api/page-detail?locale='.$lang.'&page_slug='.$_GET['page_slug']);
$privacypolicy = $privacypolicy->getData();
?>
<div>
	<div class="static-content">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="page-title"><?php echo $privacypolicy[0]->name; ?></h3>
					</div>
					<div class="col"><?php echo $privacypolicy[0]->content; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once 'footer.php'; ?>