<?php
//ini_set('display_errors', 1);
include_once '../apis/apis.php';
$slug = 'trendings';
$type = isset($_GET['type']) ? $_GET['type'] : 'postall';
$trends = new Apis('https://cms.crictoday.com/api/specials-post?locale='.$lang.'&specials_slug=trending&type='.$type);
$trends = $trends->getData();

$metatitle = 'Trendings';
//$metadesc = $privacypolicy[0]->content;
$metaimage = $trends->posts[0]->image;
//$metakeywords = $privacypolicy[0]->meta_keywords;

include_once '../header.php';
?>
<div class="in-header-tab">
	<?php include_once "../common/special-head.php"; ?>
</div>

<div class="inner-page">
		<div class="ads-space">
			<div class="container trending-page">
				<div class="row ">
					<div class="col-70 fl">
					<?php 
						foreach ($trends->posts as $key => $trend) {
							$url = '';
							if ($type == 'photos') {
								$url = '/photos/'.$trend->content_slug;
							}else{
								$url = '/detail/'.$trend->content_slug;
							}
							if($key == 0){
								echo '
							<div class="listed-news">
							<!--top-list-->
							<div class="series-banner margin-botton20">
								<img class="full margin-botton20" src="'.$cms.$trend->image.'" alt="">
								<h4 class="page-title margin-botton20"><a href="'.$url.'">'.$trend->title.'</a></h4>
								<p>'.$trend->image_caption.'</p>
							</div>
							</div>';
							}
							else {
								echo '
								<div class="listed-news">
								<div class="white-bg">
								<div class="common-state sky-stage no-hover">
									<div class="news-inner">
									<amp-img width="285" height="161" layout="fixed" src="'.$cms.$trend->image.'" alt=""></amp-img>
										<div class="device-left">
										<div class="cat-title fl">'.$trend->category.'</div>
										<div class="clr"></div>
										<h4><a href="'.$url.'">'.$trend->title.'</a></h4>
										<div class="author-caption no-break">'.$trend->author.'<span>'.$trend->created_at.'</span></div>
										</div>
									</div>	
								
								</div>
							</div>
							</div>';
							}
						}
					?>
					</div>
					<div class=	"col-30 fr">
						
					</div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
<?php include_once '../footer.php'; ?>