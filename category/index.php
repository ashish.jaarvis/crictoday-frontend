<?php 
include_once '../apis/apis.php';

$slugs = $_REQUEST['category_slug'];
$slugs = explode('/', $slugs);
$slug = $slugs[0];
$subslug = isset($slugs[1]) ? $slugs[1] : '';
$photos = array();

if ($subslug == 'photos') {
	$photos = new Apis('https://cms.crictoday.com/api/photos-list?locale='.$lang.'&game='.$slug);
	$photos = $photos->getData();
	$slug = 'category/'.$slug.'/'.$subslug;
	$metatitle = ucfirst($slugs[1]);
	include_once '../header.php';

}else{

	if (isset($subslug) && !empty($subslug)) {
		$category = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=category&sccat='.$slug.'&subcat='.$subslug);
	}else{
		$category = new Apis('https://cms.crictoday.com/api/content-list?locale='.$lang.'&type=category&sccat='.$slug);
	}
		$category = $category->getData();
		$categorylist = $category->list;
    if (isset($subslug) && !empty($subslug)) {
    	$slug = 'category/'.$slug.'/'.$subslug;
		$metatitle = ucfirst($slugs[1]);
    }else{
    	$slug = 'category/'.$slug;
		$metatitle = ucfirst($slugs[0]);
    }
	//$metadesc = $privacypolicy[0]->content;
	$metaimage = $category->slider_photos_gallery[0]->sub_photo;
	//$metakeywords = $privacypolicy[0]->meta_keywords;
	include_once '../header.php';
}
if(array_key_exists('slider_photos_gallery', $category) && empty($subslug)){
		if (count($category->slider_photos_gallery) > 0 ) {
			echo '<div class="gamer-landing-slider home-slider">';
						foreach ($category->slider_photos_gallery as $slider) {
							echo '<div class="slider">		
									<img src="'.$cms.$slider->sub_photo.'" alt="">	
								</div>';
						}
					echo '</div>';
		}
	}
?>
<!-- Start Photos Category  -->
<?php if($subslug == 'photos') { ?>
<div class="photos-main-page">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col-30 fr">
					<?php
					if(is_array($photos) && count($photos) > 0) {

						foreach ($photos as $key => $photo) {
							if ($key < 1) {
								echo '<div class="latest-pinn device-show"">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="caption">'.$photo->main_photo_caption.'</span>
									</a>
									<span class="counts">'.$photo->photos_total.'</span>
									<div class="dets">
										<div class="cat-title fl">'.$language[0]->photos.'</div>
										<div class="cat-tag fr">'.$photo->super_category.'</div>
										<div class="clr"></div>
										<div class="name">'.$photo->title.'</div>
										<div class="date">'.$photo->created_at.'</div>
									</div>
								</div>';
							}else{ break; }
						}
						foreach ( array_slice($photos, 1) as $key => $photo) {
							if ($key < 10) {
								echo '<div class="latest-photo-block">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="counts">'.$photo->photos_total.'</span>
										<div class="pos">
											<div class="tag"><span class="sky">'.$photo->super_category.'</span></div>
											<div class="title-photo">'.$photo->title.'</div>
											<div class="date-photo">'.$photo->created_at.'</div>
										</div>
									</a>
								</div>';
							} else { break; }
						}
					}else{
						echo '<div class="white-bg comm-box">No Record Found.</div>';
					}
					?>
				</div>
				<div class="col-70 fl">
				<?php 
				if(is_array($photos) && count($photos) > 0) {

					foreach ($photos as $key => $photo) {
						if ($key < 1) {
							echo '<div class="latest-pinn device-none">
									<a href="/photos/'.$photo->photo_slug.'">
										<img src="'.$cms.$photo->main_photo.'" alt="" class="full">
										<span class="caption">'.$photo->main_photo_caption.'</span>
									</a>
									<span class="counts">'.$photo->photos_total.'</span>
									<div class="dets">
										<div class="cat-title fl">'.$language[0]->photos.'</div>
										<div class="cat-tag fr">'.$photo->super_category.'</div>
										<div class="clr"></div>
										<div class="name">'.$photo->title.'</div>
										<div class="date">'.$photo->created_at.'</div>
									</div>
								</div>';
						}else{ break; }
					}

					foreach (array_slice($photos, 1, 15) as $key => $photo) {
						
							echo '<div class="listed-news">
									<div class="white-bg">
										<div class="common-state sky-stage no-hover">						
											<div class="news-inner">
											<span class="counts">'.$photo->photos_total.'</span>
											<amp-img data-amp-auto-lightbox-disable  src="'.$cms.$photo->main_photo.'" alt="'.$photo->super_category.'" width="285" height="161" layout="fixed"></amp-img>
												<div class="device-left">
												<div class="cat-title fl">'.$language[0]->photos.'</div>
												<div class="cat-tag fr">'.$photo->super_category.'</div>
												<div class="clr"></div>
												<h4><a href="/photos/'.$photo->photo_slug.'">'.$photo->title.'</a></h4>
												<div class="author-caption no-break">'.$photo->author.'<span>'.$photo->created_at.'</span></div>
												</div>
												<div class="clr"></div>
											</div>	
										
										</div>
									</div>
								</div>';
						
					}
				}else{
						echo '<div class="white-bg comm-box">No Record Found.</div>';
					}
				?>				

				</div>
				<div class="clr"></div>
			</div>
		</div>
	</div>
</div>
<?php } else { ?>
<!-- End Photos Category  -->
<div class="home-container ads-space">
	<div class="container">
		<?php if (isset($subslug) && !empty($subslug)) { 
			echo '<div class="subcat-name ng-binding ng-scope">'.ucfirst($subslug).'</div>';
		 } ?>
		<div class="row">
			<div class="col-70 fl">

				<?php 
					if (isset($category->list)) {
						echo '<div id="listed-cats-news">';
							foreach ($category->list as $key=>$listing) {
								if($key < 10){	
									echo '<div class="listed-news"><div class="white-bg">
											<div class="common-state blue-stage no-hover">
											
												<div class="news-inner">
												<amp-img data-amp-auto-lightbox-disable  src="'.$cms.$listing->image.'" width="285" height="161" layout="fixed" alt="'.$listing->category.'"></amp-img>
													<div class="device-left">
													<div class="cat-title fl">'.$listing->category.'</div>
													<div class="cat-tag fr">'.$listing->super_category.'</div>
													<div class="clr"></div>
													<h4><a href="/detail/'.$listing->content_slug.'">'.$listing->title.'</a></h4>
													<div class="author-caption no-break">'.$listing->author.'<span>'.$listing->created_at.'</span></div>
													</div>
													<div class="clr"></div>
												</div>	
											
											</div>
										</div></div>';
									}else{
										break;
									}
								}
						echo '</div>';
						$total = ceil(count($category->list)/10);
						if ($total > 0) {

							echo '<ul class="pagination ng-scope" id="catnewspagination">
								<li class="ng-scope"><a onclick="return categotopage(0, this);">«</a></li>
								<li class="ng-scope"><a onclick="return categotoprevpage();">‹</a></li>';
								$j = 0;
								for ($i = 1; $i <= $total; $i++)
								{
								    if ($i == 1)
								    {
								        echo '<li class="ng-scope page-number active"><a id="' . $j . '" onclick="return categotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
								    }
								    else
								    {
								        echo '<li class="ng-scope page-number"><a id="' . $j . '" onclick="return categotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
								    }
								    $j++;
								}
							echo '<li class="ng-scope"><a onclick="return categotonextpage();">›</a></li>
								<li class="ng-scope"><a onclick="return categotopage(4, this);">»</a></li>
							</ul>';
						}	

					}else{
						echo '<div class="white-bg comm-box">'.$category->message.'</div>';
					}
				?>
		
			</div>
			<?php include_once '../common/specials.php' ?>
			<div class="clr"></div>		
	
		</div>
	</div>
</div>
<?php } ?>
<script type="text/javascript">
	/*********************Category List Pagination*****************/
$(window).load(function(){
	var currPage = localStorage.getItem('catpage');
	//$('#catnewspagination li a#'+currPage).trigger('click');
});	
function catgetposts(page, perpage, offset, ele){
	let newslist = '';
	<?php
	$slugs = $_REQUEST['category_slug'];
	$slugs = explode('/', $slugs);
	$slug = $slugs[0];
	$subslug = isset($slugs[1]) ? $slugs[1] : '';
	if (isset($subslug) && !empty($subslug)) { ?>
		var apiurl = 'https://cms.crictoday.com/api/content-list?locale=en&type=category&sccat=<?php echo $slug; ?>&subcat=<?php echo $subslug; ?>&limit=10&offset='+offset;
	<?php }else{ ?>
			var apiurl = 'https://cms.crictoday.com/api/content-list?locale=en&type=category&sccat=<?php echo $slug; ?>&limit=10&offset='+offset;
	<?php } ?>
	axios.get(apiurl)
	  .then(function (response) {
	    if (response.data.list.length > 0) {
	    	response.data.list.map(news => {
	    		newslist +='<div class="listed-news"><div class="white-bg"><div class="common-state sky-stage no-hover"><div class="news-inner"><amp-img data-amp-auto-lightbox-disable  srcset="https://cms.crictoday.com/'+news.image+'" src="https://cms.crictoday.com/'+news.image+'" alt="'+news.title+'" width="285" height="161" layout="fixed"></amp-img><div class="device-left"><div class="cat-title fl">'+news.category+'</div><div class="cat-tag fr">'+news.super_category+'</div><div class="clr"></div><h4><a href="/detail/'+news.content_slug+'">'+news.title+'</a></h4><div class="author-caption no-break">'+news.author+'<span>'+news.created_at+'</span></div></div><div class="clr"></div></div></div></div></div>';
	    	});
	    }
	    jQuery('#listed-cats-news').html(newslist);
	    jQuery('#catnewspagination .ng-scope').removeClass('active');
	    jQuery(ele).parent().addClass('active');
	    localStorage.setItem("catpage", page);
	  })
	  .catch(function (error) {
	    // handle error
	    console.log(error);
	  })
}

function categotopage(page, ele){
	var perpage = 10;
	var offset = page*perpage;
		var ele = ele;
	if (page == '0') {
		ele = jQuery('#catnewspagination li:eq(2) a');
	}if (page == '4') {
		ele = jQuery('#catnewspagination li:eq(6) a');
	}
	catgetposts(page, perpage, offset, ele);
	 
}
function categotoprevpage(){
	var getcurrpage = parseInt(jQuery('#catnewspagination .ng-scope.active a').attr('id'));

	if (getcurrpage>0) {
		var page = getcurrpage-1;
		var perpage = 10;
	    var offset = page*perpage;
	    var prevpage = jQuery('#catnewspagination li:eq('+eval(getcurrpage+1)+') a');
	    catgetposts(page, perpage, offset, prevpage);
	}
}
function categotonextpage(){
	var getcurrpage = parseInt(jQuery('#catnewspagination .ng-scope.active a').attr('id'));

	if (getcurrpage<4) {
		var page = getcurrpage+1;
		var perpage = 10;
	    var offset = page*perpage;
	    var nextpage = jQuery('#catnewspagination li:eq('+eval(getcurrpage+3)+') a');

	    catgetposts(page, perpage, offset, nextpage);
	}
}
</script>
<?php
include_once '../footer.php';
?>