<div class="home-container ads-space">
	<div class="container">
		<div class="row">
			<div class="col-70 fl">
				<div class="player-performance">
					<div class="widget">
						<div class="performance-title"><?php echo $language[0]->explorelabel.' '.$player[0]->name.' '. $language[0]->explore; ?></div>
						<div class="player-performance-tab">
							<a href="javascript:void(0)" class="active" id="bating"><?php echo $language[0]->bating; ?></a>
							<a href="javascript:void(0)" id="bowling"><?php echo $language[0]->bowling; ?></a>
							<div class="clr"></div>
						</div>
						<div class="per-5-content content-per-bating">
							<table class="player-performance-table">
								<tr>
									<th><?php echo $language[0]->format; ?></th>
									<th>M</th>
									<th>I</th>
									<th>N/O</th>
									<th>R</th>
									<th>HS</th>
									<th>100s </th>
									<th>50s</th>
									<th>4s</th>
									<th>6s</th>
									<th>AVG</th>
									<th>S/R</th>
								</tr>
								<?php 
									if (count($player[0]->record_explore_bating)) {
										foreach ($player[0]->record_explore_bating as $value) {
											echo '<tr>';
											if($value->match_type == 'TEST'){
												echo '<td>'.$language[0]->test.'</td>';
											}if($value->match_type == 'ODI'){
												echo '<td>'.$language[0]->odi.'</td>';
											}if($value->match_type == 'T20'){
												echo '<td>'.$language[0]->t20.'</td>';
											}if($value->match_type == 'IPL'){
												echo '<td>'.$language[0]->ipl.'</td>';
											}
									echo '		
											<td>'.$value->bating_mat.'</td>
											<td>'.$value->bating_inns.'</td>
											<td>'.$value->bating_not_out.'</td>
											<td>'.$value->bating_runs.'</td>
											<td>'.$value->bating_hs.'</td>
											<td>'.$value->bating_100s.'</td>
											<td>'.$value->bating_50s.'</td>
											<td>'.$value->bating_4s.'</td>
											<td>'.$value->bating_6s.'</td>
											<td>'.$value->bating_avg.'</td>
											<td>'.$value->bating_sr.'</td>
										</tr>';
										}
									}
								?>
							</table>
						</div>
						<div class="per-5-content content-per-bowling">
							<table class="player-performance-table">
								<tr>
									<th><?php echo $language[0]->format; ?></th>
									<th>M</th>
									<th>I</th>
									<th>R</th>
									<th>W</th>
									<th>BB</th>
									<th>AVG</th>
									<th>Econ</th>
								</tr>
								<?php
									if (count($player[0]->record_explore_bowling)) {
										foreach ($player[0]->record_explore_bowling as $value) {
											echo '<tr>';
											if($value->match_type == 'TEST'){
												echo '<td>'.$language[0]->test.'</td>';
											}if($value->match_type == 'ODI'){
												echo '<td>'.$language[0]->odi.'</td>';
											}if($value->match_type == 'T20'){
												echo '<td>'.$language[0]->t20.'</td>';
											}if($value->match_type == 'IPL'){
												echo '<td>'.$language[0]->ipl.'</td>';
											}
									echo '		
											<td>'.$value->bowling_mat.'</td>									
											<td>'.$value->bowling_inns.'</td>
											<td>'.$value->bowling_runs.'</td>
											<td>'.$value->bowling_wkts.'</td>
											<td>'.$value->bowling_bb.'</td>
											<td>'.$value->bowling_ave.'</td>
											<td>'.$value->bowling_econ.'</td>
										</tr>';
										}
									}
								?>
							</table>
						</div>
					</div>
					<div class="widget">
						<div class="performance-title"><?php echo $language[0]->careerinformation; ?></div>
						<table class="player-career-info">
							<?php
									if (count($player[0]->record_information)) {
										foreach ($player[0]->record_information as $value) {
											echo '<tr>';
											if($value->match_type == 'TEST'){
												echo '<td>'.$language[0]->test.'</td>';
											}if($value->match_type == 'ODI'){
												echo '<td>'.$language[0]->odi.'</td>';
											}if($value->match_type == 'T20'){
												echo '<td>'.$language[0]->t20.'</td>';
											}if($value->match_type == 'IPL'){
												echo '<td>'.$language[0]->ipl.'</td>';
											}
									echo '		
											<td>'.$language[0]->debut.': '.$value->debut.'<br>
									        '.$language[0]->lastplayed.': '.$value->last_played.'</td>
										</tr>';
										}
									}


								?>
										</table>
									</div>

									<div class="widget">
										<div class="performance-title"><?php echo $language[0]->last5performances; ?></div>
										<div class="player-performance-tab">
											<a href="javascript:void(0)" class="active" id="test"><?php echo $language[0]->test; ?></a>
											<a href="javascript:void(0)" id="odi"><?php echo $language[0]->odi; ?></a>
											<a href="javascript:void(0)" id="t20"><?php echo $language[0]->t20; ?></a>
											<a href="javascript:void(0)" id="ipl"><?php echo $language[0]->ipl; ?></a>
											<div class="clr"></div>
										</div>
										<div class="per-5-content content-per-test">
											<table class="player-performance-table">
												<tr>
													<th><?php echo $language[0]->bating; ?></th>
													<th><?php echo $language[0]->bowling; ?></th>
													<th><?php echo $language[0]->opposition; ?></th>
													<th><?php echo $language[0]->matchdate; ?></th>
												</tr>
												<?php
												$record = $player[0]->record_performances;
												$record = array_filter($record, function($data){
													if ($data->match_type == 'TEST') {
														return true;
													}
													return false;
												});
												foreach ($record as $value) {
													echo '<tr><td>'.$value->batting.'</td>
													<td>'.$value->bowling.'</td>
													<td>'.$value->opposition.'</td>
													<td>'.$value->match_date.'</td></tr>';
												}
												?>												
											</table>
										</div>
										<div class="per-5-content content-per-odi">
											<table class="player-performance-table">
												<tr>
													<th><?php echo $language[0]->bating; ?></th>
													<th><?php echo $language[0]->bowling; ?></th>
													<th><?php echo $language[0]->opposition; ?></th>
													<th><?php echo $language[0]->matchdate; ?></th>
												</tr>
												<?php
												$record = $player[0]->record_performances;
												$record = array_filter($record, function($data){
													if ($data->match_type == 'ODI') {
														return true;
													}
													return false;
												});
												foreach ($record as $value) {
													echo '<tr><td>'.$value->batting.'</td>
													<td>'.$value->bowling.'</td>
													<td>'.$value->opposition.'</td>
													<td>'.$value->match_date.'</td></tr>';
												}
												?>
											</table>
										</div>
										<div class="per-5-content content-per-t20">
											<table class="player-performance-table">
												<tr>
													<th><?php echo $language[0]->bating; ?></th>
													<th><?php echo $language[0]->bowling; ?></th>
													<th><?php echo $language[0]->opposition; ?></th>
													<th><?php echo $language[0]->matchdate; ?></th>
												</tr>
												<?php
												$record = $player[0]->record_performances;
												$record = array_filter($record, function($data){
													if ($data->match_type == 'T20') {
														return true;
													}
													return false;
												});
												foreach ($record as $value) {
													echo '<tr><td>'.$value->batting.'</td>
													<td>'.$value->bowling.'</td>
													<td>'.$value->opposition.'</td>
													<td>'.$value->match_date.'</td></tr>';
												}
												?>
											</table>
										</div>
										<div class="per-5-content content-per-ipl">
											<table class="player-performance-table">
												<tr>
													<th><?php echo $language[0]->bating; ?></th>
													<th><?php echo $language[0]->bowling; ?></th>
													<th><?php echo $language[0]->opposition; ?></th>
													<th><?php echo $language[0]->matchdate; ?></th>
												</tr>
												<?php
												$record = $player[0]->record_performances;
												$record = array_filter($record, function($data){
													if ($data->match_type == 'IPL') {
														return true;
													}
													return false;
												});
												foreach ($record as $value) {
													echo '<tr><td>'.$value->batting.'</td>
													<td>'.$value->bowling.'</td>
													<td>'.$value->opposition.'</td>
													<td>'.$value->match_date.'</td></tr>';
												}
												?>
											</table>
										</div>
									</div>
								</div>	
							</div>
							<div class="col-30 fr">
								<?php include_once '../common/playersranking.php'; ?>
							</div>
							<div class="clr"></div>		

						</div>
					</div>
				</div>		