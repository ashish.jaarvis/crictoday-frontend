<?php
include_once '../apis/apis.php'; 
$slugs = $_REQUEST['player_slug'];
$slugs = explode('/', $slugs);
$slug = $slugs[0];
$subslug = isset($slugs[1]) ? $slugs[1] : '';
$page = new Apis('https://cms.crictoday.com/api/player-detail?locale='.$lang.'&player_slug='.$slug);
$page = $page->getData();
$page = get_object_vars($page);

if(isset($subslug) && $subslug == 'detail'){
	$player = new Apis('https://cms.crictoday.com/api/player-records?locale='.$lang.'&player_slug='.$slug);
    $player = $player->getData();
    $slug = 'player/'.$slug.'/detail';
    $metatitle = $player[0]->name;
	$metadesc = $player[0]->content;
	$metakeywords = $player[0]->meta_keywords;
	include_once '../header.php'; 
	include_once 'detail.php';

}else {
	$slug = 'player/'.$slug;
	$metatitle = $page[0]->name;
	$metadesc = $page[0]->description;
	$metaimage = $page[0]->photo_path;
	//$metakeywords = $privacypolicy[0]->meta_keywords;
 	include_once '../header.php'; 
?>
<div class="home-container ads-space">
	<div class="container">
		<div class="row">
			<div class="col-70 fl">
				<div class="player-profile">
					<div class="white-bg">
						<div class="player-head">
							<img src="<?php echo $cms.$page[0]->photo_path; ?>" alt="" class="image-player">
							<div class="player-name"><?php echo $page[0]->name; ?> <span><?php echo $page[0]->country_name; ?></span></div>
							<p><?php echo $page[0]->description; ?></p>
							
							<div class="funll-pro-div">
							<a href="/player/<?php echo $page[0]->player_slug; ?>/detail/" class="view-full-profile view-full-profile-btn"><?php echo $language[0]->viewfullprofile; ?></a>
							</div>
						</div>
						<table class="player-table">
							<tr>
								<td><?php echo $language[0]->fullname; ?></td>
								<td><?php echo $page[0]->name; ?></td>
							</tr>
							<tr>
								<td><?php echo $language[0]->age; ?></td>
								<td><?php echo $page[0]->dob; ?></td>
							</tr>
							<tr>
								<td><?php echo $language[0]->nationalside; ?></td>
								<td><?php echo $page[0]->country_name; ?></td>
							</tr>
							<tr>
								<td><?php echo $language[0]->battingstyle; ?></td>
								<td><?php echo $page[0]->batting; ?></td>
							</tr>
							<tr>
								<td><?php echo $language[0]->bowlingstyle; ?></td>
								<td><?php echo $page[0]->bwoling; ?></td>
							</tr>

							<tr>
								
								<td></td>
							</tr>
						</table>
					</div>
				</div>
				<?php
				    if(count($page["relative_posts"]) > 0){
						foreach ($page["relative_posts"] as $key => $post) {
							echo '<div class="listed-news">
									<div class="white-bg">
										<div class="common-state blue-stage no-hover">						
											<div class="news-inner">
											<img src="'.$cms.$post->image.'" alt="">
												<div class="device-left">
												<div class="cat-title fl">'.$post->category.'</div>
												<div class="cat-tag fr">'.$post->super_category.'</div>
												<div class="clr"></div>
												<h4><a href="/detail/'.$post->content_slug.'">'.$post->title.'</a></h4>
												<div class="author-caption no-break">'.$post->author.'<span>'.$post->created_at.'</span></div>
												</div>
												<div class="clr"></div>
											</div>	
										
										</div>
									</div>
								</div>';
						}	
					}
					if (count($page["relative_photos"]) > 0) {
						foreach ($page["relative_photos"] as $key => $photo) {
							echo '<div class="listed-news">
									<div class="white-bg">
										<div class="common-state sky-stage no-hover">						
											<div class="news-inner">
											<span class="counts">'.$photo->photos_total.'</span>
											<img src="'.$cms.$photo->main_photo.'" alt="">
												<div class="device-left">
												<div class="cat-title fl">'.$language[0]->photos.'</div>
												<div class="cat-tag fr">'.$photo->super_category.'</div>
												<div class="clr"></div>
												<h4><a href="/photos/'.$photo->photo_slug.'">'.$photo->title.'</a></h4>
												<div class="author-caption no-break">'.$photo->author.'<span>'.$photo->created_at.'</span></div>
												</div>
												<div class="clr"></div>
											</div>	
										
										</div>
									</div>
								</div>';
						}
					}
				?>

			</div>
			<div class="col-30 fr">
				<?php include_once '../common/playersranking.php'; ?>
			</div>
			<div class="clr"></div>		

		</div>
	</div>
</div>
<?php }

include_once '../footer.php'; ?>