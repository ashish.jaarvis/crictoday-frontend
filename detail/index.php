<?php
ini_set('display_errors', 1);
include_once '../apis/apis.php';

$slug = isset($_GET['content_slug']) ? $_GET['content_slug'] : null;

if ($slug) {
	$details = new Apis('https://cms.crictoday.com/api/content-detail?locale='.$lang.'&content_slug='.$slug);
	$lazyloaddetails = new Apis('https://cms.crictoday.com/api/content-lazyload?locale='.$lang.'&limit=5');
	$details = $details->getData();
	$lazyloaddetails = $lazyloaddetails->getData();
	$details = get_object_vars($details);
	$slug = 'detail/'.$_GET['content_slug'];
}

$metatitle = $details[0]->title;
$metadesc = $details[0]->content;
$metaimage = $details[0]->image;
$metakeywords = $details[0]->meta_keywords;

include_once '../header.php';
$slug = isset($_GET['content_slug']) ? $_GET['content_slug'] : null;
?>
<div class="news-row first-row" id="<?php echo $slug; ?>">
	<div class="banner white-bg">
		<div class="fixed-container">
		 <amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/<?php echo $details[0]->image; ?>" alt="<?php echo $details[0]->image_caption; ?>" alt="<?php echo $details[0]->image_caption; ?>" width="2" height="1" layout="responsive" class="full"></amp-img>
		</div>
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col banner-res"><p><?php echo $details[0]->image_caption; ?></p></div>
				</div>
			</div>
		</div>
	</div>
	<div class="ads-space white-bg">
		<div class="container">
			<div class="row news-detail">
				<div class="col">

					<!--5star-->
					<?php if($details[0]->star_rating == "5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--5star-->

					<!--4.5star-->
					<?php if($details[0]->star_rating == "4.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--4.5star-->

					<!--4star-->
					<?php if($details[0]->star_rating == "4") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--4star-->

					<!--3.5star-->
					<?php if($details[0]->star_rating == "3.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--3.5star-->

					<!--3star-->
					<?php if($details[0]->star_rating == "3") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--3star-->

					<!--2.5star-->
					<?php if($details[0]->star_rating == "2.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--2.5star-->

					<!--2star-->
					<?php if($details[0]->star_rating == "2") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--2star-->

					<!--1.5star-->
					<?php if($details[0]->star_rating == "1.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--1.5star-->

					<!--1star-->
					<?php if($details[0]->star_rating == "1") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--1star-->

					<!--0.5star-->
					<?php if($details[0]->star_rating == "0.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--0.5star-->

					<!--0star-->
					<?php if($details[0]->star_rating == "0" || $details[0]->star_rating == "") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php }  ?>
					<!--0star-->
					
				</div>
				<div class="col blue-stage no-hover">
					<div class="cat-title"><a href="/category/<?php echo strtolower($details[0]->super_category).'/'.strtolower($details[0]->category_slug); ?>" target="_self"><?php echo $details[0]->category; ?></a> <a class="catsname" href="/category/<?php echo strtolower($details[0]->super_category); ?>" target="_self"><span class="cat-tag fr"><?php echo $details[0]->super_category_name; ?></span></a> </div>
					<h4 class="page-title margin-botton20"><?php echo $details[0]->title; ?></h4>
				</div>
				<div class="col-50 col-xs-full fl">
					<div class="update-news">
						<div class="update-logo"><a href="/author/<?php echo $details[0]->author_slug; ?>">
						<?php if($details[0]->author_image != null || $details[0]->author_image != "") { ?>
							<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/<?php echo $details[0]->author_image; ?>" layout="responsive" width="1" height="1" alt=""></amp-img>
						<?php } else { 
							echo '<span class="nouseravatar">'.substr($details[0]->author, 0,1).'</span>';
						 } ?>
						 </a></div>
						<div class="update-detail">
							<span><a href="/author/<?php echo $details[0]->author_slug; ?>"><?php echo $details[0]->author; ?></a></span>
							Updated: <?php echo $details[0]->created_at; ?>
							<div class="author_description"><?php echo $details[0]->author_description; ?></div>
						</div>
					</div>
				</div>
				<div class="col-50 share-col col-xs-full fr">
					<?php if($details[0]->engagement != "") { ?>
						<span><strong><?php echo $details[0]->engagement; ?></strong> Engagement</span> 
					<?php } ?>
					<?php
						echo '<a href="http://www.facebook.com/sharer.php?u='.$domain.'detail/'.$slug.'&t='.$details[0]->title.'" target="_blank" data-network="facebook" class="fa st-custom-button">Facebook</a> 
							<a target="_blank" href="https://twitter.com/share?url='.$domain.'detail/'.$slug.'&text='.$details[0]->title.'" data-network="twitter" class="tw st-custom-button">Twitter</a>
							<a href="https://api.whatsapp.com/send?text='.$domain.'detail/'.$slug.'" target="_blank" data-action="share/whatsapp/share" class="whatsapp st-custom-button">whatsapp</a> 
							<a href="mailto:?subject='.$details[0]->title.'&body='.$domain.'detail/'.$slug.'" target="_blank" data-network="email" class="mail t-custom-button">Email</a> ';
					?>
					
				</div>
				<div class="clr"></div>		
			</div>
		</div>
	</div>
	<div class="inner-page">
		<div class="ads-space2">
			<div class="container">
				<div class="row">

					<div class="col">
						<div class="hightlight margin-botton40">
							<?php
								if ($details[0]->highlight_content != null) {
									echo '<div class="listed-news big shadow">
											<div class="white-bg">
												<div class="news-inner">
													<div class="big-sec">
														<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/'.$details[0]->highlight_image.'"  layout="responsive" width="2" height="1"></amp-img>
													</div>
													<h4><span>HIGHLIGHTS</span></h4>
													<div>'.$details[0]->highlight_content.'</div>
												</div>
											</div>
										</div>';
								}
							?>
							<div class="body-content"><?php echo $details[0]->content; ?></div>
								

<!-- {{lang_read_link}} 
{{lang_type}} 

{{lang}}

 --> 				
<!-- 				<div class="detail-read-link" ng-if="lang_read_link =='1'">
<a ng-if="!lang" class="" href="javascript:void(0)" ng-click="changeLangDetail('detail/'+content_slug+'?lang=hi')">Read in Hindi</a>
<a ng-if="lang" class="" href="javascript:void(0)" ng-click="changeLangDetail('detail/'+content_slug+'?lang=en')">Read in English</a>
</div> --> </div>
					</div>
					
					<div class="col">
						<form name="subsform" id="singlesubsform" autocomplete="off" novalidate>
						<div class="news-update shadow margin-botton40">
							<h5>Get Daily Updates From CricToday</h5>
							<p>Subscribe and get the latest Sports News delivered to your inbox.</p>
							<div class="input">
								<input type="email"  autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" required="" name="email" id="email" placeholder="Please enter your email id">
							</div>
							<div class="submit">
								<input type="button" class="bluebutton" value="SubScribe" onclick="singlesubscribefun()">
							</div>
							<div class="clr"></div>
							<div id="showSingleSubsErrors"></div>
						</div>
						</form>

						<?php if(isset($_COOKIE["token"])) { ?>
						<div>
						<form name="commentform" id="commentform" autocomplete="off" novalidate>
						<div class="input margin-botton20">
							<label class="blue">Comments(<?php echo count($details[0]->comments); ?>)</label>
							<input type="hidden" name="name" value="<?php echo $_COOKIE['username'] ?? ''; ?>">
							<input type="hidden" name="email" value="<?php echo $_COOKIE['email'] ?? ''; ?>">
							<input type="hidden" name="post_id" value="<?php echo $details[0]->id; ?>">
							<input type="hidden" name="post_name" value="<?php echo $details[0]->title; ?>">
							<textarea placeholder="Start writing your comment..." required="" name="comment" id="comment"></textarea>
							<div id="showCommentErrors"></div>
						</div>
						<div class="center margin-botton20">
							<div class="comment-col">
								<button class="button comment-icon" type="button" value="Submit" onclick="commentsfun()">Submit Comment</button>
							</div> 
						</div>
						</form>
						<?php if(count($details[0]->comments) > 0) { 
							foreach($details[0]->comments as $comment) { ?>
						<div class="white-bg comm-box">
							<div class="name fl"><?php echo $comment->user_name; ?></div>
							<div class="date fr"><?php echo $comment->created_at; ?></div>
							<div class="clr"></div>
							<?php echo $comment->comment; ?>
						</div>
					<?php } 
						} 
					else { ?>
						<div class="white-bg comm-box">No Comments Found</div>
					<?php } ?>
						</div>
					<?php } else { ?>
						<div class="center">
							<div class="comment-col">
								<button class="button comment-icon login-open">Login and Comment</button>
							</div>
						</div>
					<?php } ?>






					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<?php  foreach ($lazyloaddetails as $lazydetail) { 
		if($lazydetail->lazy_content_slug != $slug) { ?>

<div class="news-row lazyData" id="<?php echo $lazydetail->lazy_content_slug; ?>">
	<div class="ads-space white-bg">
		<div class="container">
			<div class="row news-detail">
				<div class="col">
					<!--5star-->
					<?php  if($details[0]->star_rating == "5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--5star-->

					<!--4.5star-->
					<?php if($details[0]->star_rating == "4.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--4.5star-->

					<!--4star-->
					<?php if($details[0]->star_rating == "4") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--4star-->

					<!--3.5star-->
					<?php if($details[0]->star_rating == "3.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--3.5star-->

					<!--3star-->
					<?php if($details[0]->star_rating == "3") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--3star-->

					<!--2.5star-->
					<?php if($details[0]->star_rating == "2.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--2.5star-->

					<!--2star-->
					<?php if($details[0]->star_rating == "2") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--2star-->

					<!--1.5star-->
					<?php if($details[0]->star_rating == "1.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--1.5star-->

					<!--1star-->
					<?php if($details[0]->star_rating == "1") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/full-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--1star-->

					<!--0.5star-->
					<?php if($details[0]->star_rating == "0.5") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/half-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php } ?>
					<!--0.5star-->

					<!--0star-->
					<?php if($details[0]->star_rating == "0" || $details[0]->star_rating == "") { ?>
						<div class="rating-for-detail">
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
							<amp-img data-amp-auto-lightbox-disable  width="19" height="19" layout="fixed" src="<?php echo $domain; ?>assets/images/no-star.png" alt=""></amp-img>
						</div>
					<?php }  ?>
					<!--0star-->



				</div>
				<div class="col sky-stage no-hover">
					<div class="cat-title">
						<a href="/category/<?php echo strtolower($lazydetail->lazy_super_category_slug).'/'.strtolower($lazydetail->lazy_category_slug); ?>" target="_self"><?php echo $lazydetail->lazy_category; ?></a> 

						<a class="catsname" href="/category/<?php echo strtolower($lazydetail->lazy_super_category_slug); ?>" target="_self"><span class="cat-tag fr"><?php echo $lazydetail->lazy_super_category; ?></span></a> 
					</div>

					<div class="clr"></div>
					<h4 class="page-title margin-botton20"><?php echo $lazydetail->lazy_title; ?></h4>
				</div>
				<div class="col-50 col-xs-full fl">
					<div class="update-news">
						<div class="update-logo"><a href="/author/<?php echo $lazydetail->lazy_author_slug; ?>">
						<?php if($lazydetail->lazy_author_image != null || $lazydetail->lazy_author_image != "") { ?>
							<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/<?php echo $lazydetail->lazy_author_image; ?>" layout="responsive" width="1" height="1" alt=""></amp-img>
						<?php } else { 
							echo '<span class="nouseravatar">'.substr($lazydetail->lazy_author, 0,1).'</span>';
						 } ?>
						 </a></div>

						<div class="update-detail">
							<span><a href="/author/<?php echo $lazydetail->lazy_author_slug; ?>"><?php echo $lazydetail->lazy_author; ?></a></span>
							Updated: <?php echo $lazydetail->lazy_created_at; ?>
							<div class="author_description"><?php echo $lazydetail->lazy_author_description; ?></div>
						</div>
					</div>
				</div>
				<div class="col-50 share-col col-xs-full fr">
					<?php  if($lazydetail->engagement != "") { ?>
								<span><strong><?php echo $lazydetail->engagement; ?></strong> Engagement</span> 
					<?php  } ?>
					<?php
						echo '<a href="http://www.facebook.com/sharer.php?u='.$domain.'detail/'.$lazydetail->lazy_content_slug.'&t='.$lazydetail->lazy_title.'" target="_blank" data-network="facebook" class="fa st-custom-button">Facebook</a> 
							<a target="_blank" href="https://twitter.com/share?url='.$domain.'detail/'.$lazydetail->lazy_content_slug.'&text='.$lazydetail->lazy_title.'" data-network="twitter" class="tw st-custom-button">Twitter</a>
							<a href="whatsapp://send?text='.$domain.'detail/'.$lazydetail->lazy_content_slug.'" target="_blank" data-action="share/whatsapp/share" class="whatsapp st-custom-button">whatsapp</a> 
							<a href="mailto:?subject='.$lazydetail->lazy_title.'&body='.strip_tags($lazydetail->lazy_content).'" target="_blank" data-network="email" class="mail t-custom-button">Email</a> ';
					?>
				</div>
				<div class="clr"></div>	
				</div>
				
				
					
			</div>
		</div>
		<div class="banner">
		<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/<?php echo $lazydetail->lazy_image; ?>" alt="" width="1" height="0.5" layout="responsive"></amp-img>
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col banner-res"><p><?php echo $lazydetail->lazy_image_caption; ?></p></div>
				</div>
			</div>
		</div>
	</div>
	

	<div class="inner-page">
		<div class="ads-space2">
			<div class="container">
				<div class="row">
					<div class="col">
						<div class="hightlight margin-botton40">
							<?php 
								if ($lazydetail->lazy_highlight_content) {
										echo '<div class="listed-news big shadow">
											<div class="white-bg">
												<div class="news-inner">
													<div class="big-sec">
														<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/'.$lazydetail->lazy_highlight_image.'" alt="" width="1" height="0.5"></amp-img>
													</div>
													<h4><span>HIGHLIGHT</span></h4>
													<div>'.$lazydetail->lazy_highlight_content.'</div>
												</div>
											</div>
										</div>';
								}
								if ($lazydetail->lazy_content) {
									echo '<div class="body-content">'.$lazydetail->lazy_content.'</div>';
								}
							?>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	</div>
<?php 
	}
}
?>	
</div>



</div>
<?php
include_once '../footer.php';
?>