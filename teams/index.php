<?php
include_once '../apis/apis.php';

$teams = new Apis('https://cms.crictoday.com/api/team?locale='.$lang.'&type=Team');
$teams = $teams->getData();

include_once '../header.php';
?>
<div>
	<!-- <div class="loader"></div> -->
	<div class="inner-page">
		<div class="ads-space">
			<div class="container">
				<div class="row ">
					<div class="col margin-botton40">
						<h3 class="page-title"><?php echo $language[0]->teams; ?></h3>
					</div>
					<?php foreach ($teams as $team) {
						echo '<div class="col-25 col-xs-33 fl margin-botton20">
									<div style="background: url('.$cms.$team->team_logo_bg.') center no-repeat;  background-size:cover;" class="team-box '.$team->team_slug.'">
										<div  class="team-circle">  <img src="'.$cms.$team->team_logo.'" /> </div>
										<div class="team-title2">'.$team->team_name.'</div>
										<a class="team-link"  href="/teams/'.$team->team_slug.'">'.$language[0]->viewteam.'</a>
									</div>
								</div>';
					} ?>				
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once '../footer.php'; ?>