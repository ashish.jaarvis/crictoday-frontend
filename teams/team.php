<?php
include_once '../apis/apis.php';
$slug = 'teams/'.$_REQUEST['team_slug'];
$teams = new Apis('https://cms.crictoday.com/api/team?locale='.$lang.'&type=Team');
$singleteam = new Apis('https://cms.crictoday.com/api/team?locale='.$lang.'&team_slug='.$_REQUEST['team_slug']);
$teams = $teams->getData();
$singleteam = $singleteam->getData();

$metatitle = $singleteam[0]->team_name;
//$metadesc = $singleteam[0]->content;
$metaimage = $singleteam[0]->team_logo;;
$metakeywords = $singleteam[0]->meta_keywords;

include_once '../header.php';
?>
<div>	
	<div class="inner-page">
		<div class="ads-space">
			<div class="container">
				<div class="row ">
					<div class="col-70 fl">
						<div class="row">
							<div class="col-33 col-xs col-xs-100 fr margin-botton20">
								<div class="res-filter-team">
									<div class="selected-team"><img src="<?php echo $cms.$singleteam[0]->team_logo; ?>" alt="<?php echo $singleteam[0]->team_name; ?>" width="20" height="20" layout="fixed"></img><?php echo $singleteam[0]->team_name; ?></div>
								<div class="teams-list">
								<?php 
									foreach ($teams as $team) {
										echo '<div class="teams-url"><a href="/teams/'.$team->team_slug.'"><img src="'.$cms.$team->team_logo.'" alt="<?php echo $singleteam[0]->team_name; ?>" width="20" height="20" layout="fixed"></img>'.$team->team_name.'</a></div>';
									}
								?>
								</div>
								</div>
							</div>
							<div class="clr"></div>
						</div>
						<h4 class="page-title margin-botton20"><?php echo $singleteam[0]->team_name; ?>
						<?php if(count($singleteam[0]->team_players) > 0){ ?>
						<div class="stage-filter">
							<input type="radio" name="teamfilter" onclick="return teamfilter('odi-team');" checked="checked" id="ODI" value="ODI">
							<label for="ODI"><?php echo $language[0]->odi; ?></label>
							<input type="radio" name="teamfilter" id="TEST" onclick="return teamfilter('test-team');" value="TEST">
							<label for="TEST"><?php echo $language[0]->test; ?></label>
							<input type="radio" name="teamfilter" id="T20" onclick="return teamfilter('t20-team');" value="T20" >
							<label for="T20"><?php echo $language[0]->t20; ?></label>
						</div>
						<?php } ?>
						</h4>
						<div class="white-bg team-bg">
							<div class="team-player">
							<?php 
								if(count($singleteam[0]->team_players) > 0){
									echo '<div id="odi-team" class="teamsfilter">';
									foreach ($singleteam[0]->team_players as $team) {
										if (strpos($team->type, "ODI") !== false) {
							?>
							
								<div class="col-50 col-xs-100 fl player-list">
									<!-- <div class="player-img" ng-if="data.full_profile==0">
										<a ng-if="data.description" href="javascript:void(0)" class="opn-p-des"><img src="{{imagesurl}}{{data.photo_path}}" alt=""></a>
										<img src="{{imagesurl}}{{data.photo_path}}" alt="" ng-if="!data.description">
									</div> -->

									<div class="player-img">
										<a href="/player/<?php echo $team->player_slug; ?>"><img src="<?php echo $cms.$team->photo_path; ?>" alt=""></a>
									</div>
									<div class="player-detail">
										<input type="hidden" class="data" value="<?php echo $team->description; ?>">
										<div class="p-info name"><strong><?php echo $team->name; ?></strong></div>
										<?php
											if ($team->dob) {
												echo '<div class="p-info"><strong>'.$language[0]->age.':</strong>'.$team->dob.'</div>';
											}if ($team->playing_roll) {
												echo '<div class="p-info"><strong>'.$language[0]->playingrole.':</strong> '.$team->playing_roll.'</div>';
											}if ($team->batting) {
												echo '<div class="p-info"><strong>'.$language[0]->bating.':</strong>'.$team->batting.'</div>';
											}if ($team->bwoling) {
												echo '<div class="p-info"><strong>'.$language[0]->bowling.':</strong>'.$team->bwoling.'</div>';
											}
										 ?>										
									</div>
								</div>							
							<?php
									}
								}
							echo '</div>';	
							echo '<div id="test-team" class="teamsfilter" style="display:none;">';
										foreach ($singleteam[0]->team_players as $team) {
										if (strpos($team->type, "TEST") !== false) {
							?>
							
								<div class="col-50 col-xs-100 fl player-list">
									<!-- <div class="player-img" ng-if="data.full_profile==0">
										<a ng-if="data.description" href="javascript:void(0)" class="opn-p-des"><img src="{{imagesurl}}{{data.photo_path}}" alt=""></a>
										<img src="{{imagesurl}}{{data.photo_path}}" alt="" ng-if="!data.description">
									</div> -->

									<div class="player-img">
										<a href="/player/<?php echo $team->player_slug; ?>"><img src="<?php echo $cms.$team->photo_path; ?>" alt=""></a>
									</div>
									<div class="player-detail">
										<input type="hidden" class="data" value="<?php echo $team->description; ?>">
										<div class="p-info name"><strong><?php echo $team->name; ?></strong></div>
										<?php
											if ($team->dob) {
												echo '<div class="p-info"><strong>'.$language[0]->age.':</strong>'.$team->dob.'</div>';
											}if ($team->playing_roll) {
												echo '<div class="p-info"><strong>'.$language[0]->playingrole.':</strong> '.$team->playing_roll.'</div>';
											}if ($team->batting) {
												echo '<div class="p-info"><strong>'.$language[0]->bating.':</strong>'.$team->batting.'</div>';
											}if ($team->bwoling) {
												echo '<div class="p-info"><strong>'.$language[0]->bowling.':</strong>'.$team->bwoling.'</div>';
											}
										 ?>										
									</div>
								</div>							
							<?php
									}
								}
							echo '</div>';	
							echo '<div id="t20-team" class="teamsfilter" style="display:none;">';
										foreach ($singleteam[0]->team_players as $team) {
										if (strpos($team->type, "T20") !== false) {
							?>
							
								<div class="col-50 col-xs-100 fl player-list">
									<!-- <div class="player-img" ng-if="data.full_profile==0">
										<a ng-if="data.description" href="javascript:void(0)" class="opn-p-des"><img src="{{imagesurl}}{{data.photo_path}}" alt=""></a>
										<img src="{{imagesurl}}{{data.photo_path}}" alt="" ng-if="!data.description">
									</div> -->

									<div class="player-img">
										<a href="/player/<?php echo $team->player_slug; ?>"><img src="<?php echo $cms.$team->photo_path; ?>" alt=""></a>
									</div>
									<div class="player-detail">
										<input type="hidden" class="data" value="<?php echo $team->description; ?>">
										<div class="p-info name"><strong><?php echo $team->name; ?></strong></div>
										<?php
											if ($team->dob) {
												echo '<div class="p-info"><strong>'.$language[0]->age.':</strong>'.$team->dob.'</div>';
											}if ($team->playing_roll) {
												echo '<div class="p-info"><strong>'.$language[0]->playingrole.':</strong> '.$team->playing_roll.'</div>';
											}if ($team->batting) {
												echo '<div class="p-info"><strong>'.$language[0]->bating.':</strong>'.$team->batting.'</div>';
											}if ($team->bwoling) {
												echo '<div class="p-info"><strong>'.$language[0]->bowling.':</strong>'.$team->bwoling.'</div>';
											}
										 ?>										
									</div>
								</div>							
							<?php
									}
								}
							echo '</div>';

							} else { 
							?>
							<div class="white-bg comm-box">No Player Found</div>
						<?php } ?>
								<div class="clr"></div>
							</div>
						</div>
					</div>
					<div class="col-30 fr">
						<div class="widget"><?php include_once '../common/playersranking.php'; ?></div>
					</div>
					<div class="clr"></div>
				</div>
			</div>
		</div>
	</div>

</div>	

<?php
include_once '../footer.php';
?>