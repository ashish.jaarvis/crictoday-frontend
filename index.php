<?php
include_once 'apis/apis.php';
include_once 'header.php';
?>
<div class="home-slider">
	<?php foreach ($homeslider as $slide)
{
    echo '
			<div class="slider-repeat" style="background:url(https://cms.crictoday.com/' . $slide->image . ') center / 100% auto no-repeat;">
				<div class="overlay">
					<div class="ads-space">
						<div class="container">
							<div class="row">
								<div class="col">
						<a href="/detail/' . $slide->content_slug .'">
							<div class="cat-tag-slider"><span class="pink">' . $slide->super_category . '</span></div>
							<h3>' . $slide->title . '</h3>
							<div class="author-caption">' . $slide->author . '<span>' . $slide->created_at . '</span></div>
						</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		';
} ?>
	
</div>
<div class="home-container ads-space">
	<div class="container">
		<div class="row">

			<div class="breaking-news">
				<div class="col">
					<div class="in-b"><span class="label">Breaking News -</span><span>{{breaking | removeHTMLTags}}</span></div>
				</div>
			</div>

			<div class="top-pinn">
				<?php
$index = 1;
if(count($hometop4post) > 0){
foreach ($hometop4post as $post)
{
    echo '<div class="col-25 col-xs fl">
								<div class="white-bg top-pinn-news">
									<a href="/detail/'.$post->content_slug.'">						
										<div class="common-state blue-stage count-'.$index.'">
											<amp-img data-amp-auto-lightbox-disable  src="https://cms.crictoday.com/' . $post->image . '" alt="' . $post->category . '" srcset="https://cms.crictoday.com/' . $post->image . '" alt="' . $post->category . '"
											width="2" height="1.1" layout="responsive" class="full"></amp-img>
											<div class="padding-15">
												<div class="cat-title white-xs fl">' . $post->category . '</div>
												<div class="cat-tag white-xs fr white-xs-border">' . $post->super_category . '</div>
												<div class="clr"></div>
												<h4 class="white-xs">' . $post->title . '</h4>
												<div class="author-caption white-xs">' . $post->author . '<span>' . $post->created_at . '</span></div>
											</div>
										</div>
									</a>
								</div>
							</div>';
    $index++;
}
}
?>
			<div class="clr"></div>
			</div>
			
			
			<div class="video-slider-home">
				<?php
foreach ($homevideo as $video)
{
    echo '<div class="col-33">
								<div class="white-bg">
									<a href="/detail/' . $video->content_slug . '" class="icon-video"><img src="https://cms.crictoday.com/' . $video->image . '" srcset="https://cms.crictoday.com/' . $video->image . '" alt="' . $video->title . '" class="full"/>
									<span><h4>' . $video->title . '</h4></span></a>
								</div>
							</div>';
}
?>
			</div>

			<div class="trending-slider-mobile">
				<div class="col">
					<div class="inside-title">Trending</div>
					<div class="inside-bg">
						<?php
							$trends = array_filter($trending, function($type){
								return $type->type === 'trending';
							});
							foreach ($trends[1]->posts as $trend)
							{
							    echo '<div><a href="/detail/' . $trend->content_slug . '">' . $trend->title . '</a></div>';
							}
						?>
					</div>
				</div>
			</div>






			<div class="col-70 fl">
				<div id="newslist">
							
							<?php
		foreach ($homelisting as $listing)
		{
		    echo '<div class="listed-news">
							<div class="white-bg">
								<div class="common-state sky-stage no-hover"><div class="news-inner">
									<amp-img data-amp-auto-lightbox-disable  srcset="https://cms.crictoday.com/' . $listing->image . '" src="https://cms.crictoday.com/' . $listing->image . '" alt="' . $listing->title . '" width="285" height="161" layout="fixed"></amp-img>
										<div class="device-left">
											<div class="cat-title fl">' . $listing->category . '</div>
											<div class="cat-tag fr">' . $listing->super_category . '</div>
											<div class="clr"></div>
											<h4><a href="/detail/' . $listing->content_slug . '">' . $listing->title . '</a></h4>
											<div class="author-caption no-break">' . $listing->author . '<span>' . $listing->created_at . '</span></div>
										</div>
										<div class="clr"></div>
									</div></div>
							</div>
						</div>';
		}
		?>
				</div>	
			<ul class="pagination ng-scope" id="newspagination">
				<li class="ng-scope"><a onclick="return gotopage(0, this);">«</a></li>
				<li class="ng-scope"><a onclick="return gotoprevpage();">‹</a></li>
				<?php
					$j = 0;
					for ($i = 1;$i <= 5;$i++)
					{
					    if ($i == 1)
					    {
					        echo '<li class="ng-scope page-number active"><a id="' . $j . '" onclick="return gotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
					    }
					    else
					    {
					        echo '<li class="ng-scope page-number"><a id="' . $j . '" onclick="return gotopage(' . $j . ', this);" class="ng-binding">' . $i . '</a></li>';
					    }
					    $j++;
					}
					?>
				<li class="ng-scope"><a onclick="return gotonextpage();">›</a></li>
				<li class="ng-scope"><a onclick="return gotopage(4, this);">»</a></li>
			</ul>					
			</div>
         	<?php include_once 'common/specials.php' ?>
			<div class="clr"></div>		
	
		</div>
	</div>
</div>
<?php
include_once 'footer.php';
?>
