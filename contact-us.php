<?php
include_once 'apis/apis.php'; 
$slug = 'contact-us';
$metatitle = $language[0]->contactus;

include_once 'header.php';
?>
<div>
<div class="inner-banner">
	<img src="assets/images/inner-banner/contact-banner.jpg" alt="">
	<div class="banner-text">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="center"><?php echo $language[0]->wego; ?></h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="inner-page">
	<div class="ads-space">
		<div class="container">
			<div class="row">
				<div class="col margin-botton40">
					<h4 class="page-title center"><?php echo $language[0]->contactus; ?></h4>
				</div>
			</div>
			<div class="input-form">
				
			<form name="contactform" id="contactform" autocomplete="off" novalidate>

				<div class="row">
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->name; ?><span>*</span></label>
							<input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox"  id="name" name="name">	
						</div>
					</div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
						<label class="labs"><?php echo $language[0]->emailid; ?><span>*</span></label>
						<input name="email" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" type="email" required="">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->designation; ?><span>*</span></label>
							<input name="designation" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" type="text" required="">
						</div>	
					</div>
					<div class="col-50 col-field-full fl">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->phoneno; ?><span>*</span></label>
							<input name="phone" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textbox" type="text" required="" maxlength="10" minlength="10">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<div class="form-row">
							<label class="labs"><?php echo $language[0]->message; ?><span>*</span></label>
							<textarea name="message" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="textarea" required=""></textarea>
						</div>
					</div>
				</div>
				<div class="row" id="showContactErrors"></div>
				<div class="row">
					<div class="col submit center">
						<button type="button" class="button-w" onclick="return contactfunc();"><span><?php echo $language[0]->submit; ?></span></button>
					</div>
				</div>
			
			</form>

			</div>


			<div class="row">
					<div class="col social-col">
						<p><?php echo $language[0]->text3; ?></p>
						<ul>
							<li><a class="fa" href="#"></a></li>
							<li><a class="tw" href="#"></a></li>
							<li><a class="lin" href="#"></a></li>
							<li><a class="in" href="#"></a></li>
							<div class="clr"></div>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="cont-col">
						<div class="col margin-botton40"><h4 class="page-title center"><?php echo $language[0]->getintouchwithus; ?></h4></div>
						<div class="col-33 col-block-contact fl">
							<div class="col-box phone">
								<p class="no-title"><?php echo $language[0]->phoneno; ?></p>
								<p><a href="tel:+911140712107">+91-11-40712107</a></p>
								<p><a href="tel:+911140712120">+91-11-40712120</a></p>
							</div>
						</div>
						<div class="col-33 col-block-contact fl">
							<div class="col-box email">
								<p class="no-title"><?php echo $language[0]->emailfax; ?></p>
								<p><a href="mailto:info@dpb.in">info@dpb.in</a></p>
								<p><a href="tel:+911141611866">+91-11-41611866</a></p>
							</div>
						</div>
						<div class="col-33 col-block-contact fl">
							<div class="col-box add">
								<p class="no-title"><?php echo $language[0]->address; ?></p>
								<p>Diamond Publication X-30,<br> Okhla Industrial Area, Phase-II<br> New Delhi India - 1100206</p>
							</div>
						</div>
						<div class="clr"></div>
					</div>
				</div>
		</div>
	</div>
</div>


</div>

<?php include_once 'footer.php'; ?>