<?php
include_once 'apis/apis.php';
$slug = 'terms-of-use';
$termsofuse = new Apis('https://cms.crictoday.com/api/page-detail?locale='.$lang.'&page_slug=terms-of-use');
$termsofuse = $termsofuse->getData();

$metatitle = $termsofuse[0]->name;
$metadesc = $termsofuse[0]->content;
$metakeywords = $termsofuse[0]->meta_keywords;

include_once 'header.php';
?>
<div>
	<div class="static-content">
		<div class="ads-space">
			<div class="container">
				<div class="row">
					<div class="col">
						<h3 class="page-title"><?php echo $termsofuse[0]->name; ?></h3>
					</div>
					<div class="col"><?php echo $termsofuse[0]->content; ?></div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include_once 'footer.php'; ?>